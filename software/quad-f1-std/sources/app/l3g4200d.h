#ifndef __L3G4200D_H__
#define __L3G4200D_H__

#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************************************
* Define for gyro sensor
*******************************************************************************/

#define L3G_WHO_AM_I           0x0F
#define L3G_CTRL_REG1          0x20
#define L3G_CTRL_REG2          0x21
#define L3G_CTRL_REG3          0x22
#define L3G_CTRL_REG4          0x23
#define L3G_CTRL_REG5          0x24
#define L3G_REFERENCE          0x25
#define L3G_OUT_TEMP           0x26
#define L3G_STATUS_REG         0x27
#define L3G_OUT_X_L            0x28
#define L3G_OUT_X_H            0x29
#define L3G_OUT_Y_L            0x2A
#define L3G_OUT_Y_H            0x2B
#define L3G_OUT_Z_L            0x2C
#define L3G_OUT_Z_H            0x2D
#define L3G_FIFO_CTRL_REG      0x2E
#define L3G_FIFO_SRC_REG       0x2F
#define L3G_INT1_CFG           0x30
#define L3G_INT1_SRC           0x31
#define L3G_INT1_THS_XH        0x32
#define L3G_INT1_THS_XL        0x33
#define L3G_INT1_THS_YH        0x34
#define L3G_INT1_THS_YL        0x35
#define L3G_INT1_THS_ZH        0x36
#define L3G_INT1_THS_ZL        0x37
#define L3G_INT1_DURATION      0x38

/**
*  \Gyroscope I2C Slave Address
*/
//#define L3G_I2C_ADDRESS         0xD1
#define L3G_I2C_ADDRESS         0x69


/**
  * @}
  */ /* end of group Gyroscope_Register_Mapping */


/**
  * @defgroup Gyroscope_Configuration_Defines
  *@{
  */

// L3G_CTRL_REG1

//Data Rate
#define L3G_ODR_100                        	((uint8_t)0x00)
#define L3G_ODR_200                        	((uint8_t)0x40)
#define L3G_ODR_400                        	((uint8_t)0x80)
#define L3G_ODR_800                        	((uint8_t)0xC0)

// Cut-off frequency varies depending on ODR. See table 22 for reference.

//	BW(1:0)		ODR=100Hz	ODR=200Hz	ODR=400Hz	ODR=800Hz
//	---------------------------------------------------------
//		00			 12.5		 12.5		 20			 30
//		01			 25			 25			 25			 25
//		10			 25			 50			 50			 50
//		11			 25			 70			110			110
#define L3G_BW_Conf1						((uint8_t)0x00)
#define L3G_BW_Conf2						((uint8_t)0x10)
#define L3G_BW_Conf3                		((uint8_t)0x20)
#define L3G_BW_Conf4                 		((uint8_t)0x30)

//Sets power on-off
#define L3G_PD_Off							((uint8_t)0x00)
#define L3G_PD_On							((uint8_t)0x08)

//Enable axis data. '11' to enable all.
#define L3G_XEN                             ((uint8_t)0x01)
#define L3G_YEN                             ((uint8_t)0x02)
#define L3G_ZEN                             ((uint8_t)0x04)
#define L3G_XYZEN                           ((uint8_t)0x07)


// L3G_CTRL_REG2

#define L3G_HPM_Normal                		((uint8_t)0x00)
#define L3G_HPM_Ref_Sig                		((uint8_t)0x10)
#define L3G_HPM_Autores               		((uint8_t)0x30)

// Cut-off frequency varies depending on ODR. See table 27 for reference.
#define L3G_HPCF_1               			((uint8_t)0x00)
#define L3G_HPCF_2                			((uint8_t)0x01)
#define L3G_HPCF_3               			((uint8_t)0x02)
#define L3G_HPCF_4               			((uint8_t)0x03)
#define L3G_HPCF_5                			((uint8_t)0x04)
#define L3G_HPCF_6               			((uint8_t)0x05)
#define L3G_HPCF_7               			((uint8_t)0x06)
#define L3G_HPCF_8                			((uint8_t)0x07)
#define L3G_HPCF_9               			((uint8_t)0x08)
#define L3G_HPCF_10               			((uint8_t)0x09)

// L3G_CTRL_REG3


// L3G_CTRL_REG4
#define L3G_Little_Endian                   ((uint8_t)0x00)
#define L3G_Big_Endian                      ((uint8_t)0x40)

#define L3G_BDU_Continous                   ((uint8_t)0x00)
#define L3G_BDU_Single                      ((uint8_t)0x80)

#define L3G_FS_250                          ((uint8_t)0x00)
#define L3G_FS_500                          ((uint8_t)0x10)
#define L3G_FS_2000                         ((uint8_t)0x20)

#define L3G_ST_Normal                       ((uint8_t)0x00)
#define L3G_ST_0             	            ((uint8_t)0x02)
#define L3G_ST_1             	            ((uint8_t)0x06)


// L3G_CTRL_REG5
#define L3G_BOOT_Normal                     ((uint8_t)0x00)
#define L3G_BOOT_Reboot                     ((uint8_t)0x80)

#define L3G_FIFO_Enable                     ((uint8_t)0x40)
#define L3G_FIFO_Disable                    ((uint8_t)0x00)

#define L3G_HP_Enable						((uint8_t)0x10)
#define L3G_HP_Disable						((uint8_t)0x00)

// L3G_FIFO_CTRL_REG


#define L3G_Gyr_OffsetX_Default				0
#define L3G_Gyr_OffsetY_Default				0
#define L3G_Gyr_OffsetZ_Default				0

#define L3G_Gyr_Gain00_Default				1
#define L3G_Gyr_Gain01_Default				0
#define L3G_Gyr_Gain02_Default				0
#define L3G_Gyr_Gain10_Default				0
#define L3G_Gyr_Gain11_Default				1
#define L3G_Gyr_Gain12_Default				0
#define L3G_Gyr_Gain20_Default				0
#define L3G_Gyr_Gain21_Default				0
#define L3G_Gyr_Gain22_Default				1

#define L3G_Gyr_Coeff						0.0001527

/**
  * @}
  */ /* end of group Gyroscope_Configuration_Defines */




/**
* @Gyroscope Init structure definition
*/

typedef struct
{

  uint8_t ODR; /*!< Output Data Rate */
  uint8_t Bandwidth;
  uint8_t Power;/*!<  L3G_PD_On, L3G_PD_Off*/
  uint8_t Axis_Enable;    /*!< Axis Enable */
  uint8_t FS;     /*!< Full Scale */
  uint8_t BDU;  /*!< Data Update mode : Continuos update or data don`t change until MSB and LSB nex reading */
  uint8_t Endianess;   /*!< Endianess */
}L3G_ConfigTypeDef;


#ifdef __cplusplus
}
#endif

#endif // __L3G4200D_H__
