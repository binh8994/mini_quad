#ifndef __APP_DATA_H__
#define __APP_DATA_H__
#include <stdint.h>
#include "app.h"

/******************************************************************************
* Commom data structure for transceiver data
*******************************************************************************/
#define FIRMWARE_PSK		0x1A2B3C4D
#define FIRMWARE_LOK		0x1234ABCD

typedef struct {
	uint32_t psk;
	uint32_t bin_len;
	uint16_t checksum;
} firmware_header_t;

typedef struct {
	float roll_cal;
	float pitch_cal;
	float yaw_cal;

	float roll_input;
	float pitch_input;
	float yaw_input;

} global_sensor_t;

extern global_sensor_t global_sensor;

#endif //__APP_DATA_H__
