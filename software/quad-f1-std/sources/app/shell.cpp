/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "../ak/ak.h"
#include "../ak/task.h"
#include "../ak/timer.h"
#include "../ak/message.h"

#include "../common/cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_shell.h"
#include "task_list.h"

#include "../driver/led/led.h"

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(const int8_t*)"reset",	shell_reset,		(const int8_t*)"reset terminal"},
	{(const int8_t*)"ver",		shell_ver,			(const int8_t*)"version info"},
	{(const int8_t*)"help",		shell_help,			(const int8_t*)"help command info"},
	{(const int8_t*)"reboot",	shell_reboot,		(const int8_t*)"reboot system"},
	/*************************************************************************/
	/* debug command */
	/*************************************************************************/

	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_ver(uint8_t* argv) {
	(void)argv;
	firmware_header_t firmware_header;
	sys_ctrl_get_firmware_info(&firmware_header);

	LOGIN_PRINT("kernel version: %s\n", AK_VERSION);
	LOGIN_PRINT("firmware checksum: %04x\n", firmware_header.checksum);
	LOGIN_PRINT("firmware length: %d\n", firmware_header.bin_len);
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_reset();
	return 0;
}
