#include "task_list.h"
#include "../ak/timer.h"

task_t app_task_table[] = {
	/*************************************************************************/
	/* SYSTEM TASK */
	/*************************************************************************/
	{TASK_TIMER_TICK_ID,	TASK_PRI_LEVEL_7,		task_timer_tick			},

	/*************************************************************************/
	/* APP TASK */
	/*************************************************************************/
	{AC_TASK_SHELL_ID		,	TASK_PRI_LEVEL_2	,	task_shell			},
	{AC_TASK_LIFE_ID		,	TASK_PRI_LEVEL_4	,	task_life			},
	{AC_TASK_RF24_ID		,	TASK_PRI_LEVEL_5	,	task_rf24			},
	{AC_TASK_SENSOR_ID		,	TASK_PRI_LEVEL_6	,	task_sensor			},

	/*************************************************************************/
	/* END OF TABLE */
	/*************************************************************************/
	{AC_TASK_EOT_ID,			TASK_PRI_LEVEL_0,		(pf_task)0			}
};
