/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef APP_H
#define APP_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************/
/*  life task define
 */
/*****************************************************************************/
/* define timer */
#define AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
#define AC_LIFE_SYSTEM_CHECK						(0)

/*****************************************************************************/
/*  shell task define
 */
/*****************************************************************************/
/* define timer */

/* define signal */
#define AC_SHELL_LOGIN_CMD							(0)
#define AC_SHELL_REMOTE_CMD							(1)

/*****************************************************************************/
/*  rf24 task define
 */
/*****************************************************************************/
/* define timer */
#define AC_RF24_IF_TIMER_PACKET_DELAY_INTERVAL		(100) /* 100 ms */

/* define signal */
/* interrupt signal */
#define AC_RF24_IRQ_TX_FAIL							(1)
#define AC_RF24_IRQ_TX_SUCCESS						(2)
#define AC_RF24_IRQ_RX_READY						(3)

/* task signal */
#define AC_RF24_INIT								(4)
#define AC_RF24_IRQ_POL								(5)

/*****************************************************************************/
/*  sensor task define
 */
/*****************************************************************************/
/* define timer */
#define AC_SENSOR_READ_INTERVAL						(500)

/* define signal */
/* task signal */
#define AC_SENSOR_INIT								(1)
#define AC_SENSOR_CALIB								(2)
#define AC_SENSOR_READ								(3)

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

/*****************************************************************************/
/*  app function declare
 */
/*****************************************************************************/
#define AC_NUMBER_SAMPLE_CT_SENSOR				3000

extern int  main_app();

#ifdef __cplusplus
}
#endif

#endif //APP_H
