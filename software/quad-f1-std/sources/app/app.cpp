/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

/* kernel include */
#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"
#include "../ak/fsm.h"

/* driver include */
#include "../driver/led/led.h"

/* app include */
#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_shell.h"
#include "task_life.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

/* common include */
#include "../common/utils.h"

static void app_start_timer();
static void app_task_init();

global_sensor_t global_sensor;

/*****************************************************************************/
/* app main function.
 */
/*****************************************************************************/
int main_app() {
	APP_PRINT("main_app() entry OK\n");

	/******************************************************************************
	* init active kernel
	*******************************************************************************/
	ENTRY_CRITICAL();
	task_init();
	task_create(app_task_table);
	EXIT_CRITICAL();

	/******************************************************************************
	* init applications
	*******************************************************************************/
	/*********************
	* hardware configure *
	**********************/
	/* init watch dog timer */
//	sys_ctrl_independent_watchdog_init();	/* 32s */
//	sys_ctrl_soft_watchdog_init(200);		/* 20s */

	SPI.begin();

	/* adc peripheral configure */
	io_cfg_adc1();
	io_cfg_i2c2();

	/* adc configure io */
	adc_batery_io_cfg();
	cfg_i2c2();

	/*********************
	* software configure *
	**********************/
	/* life led init */
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	/* start timer for application */
	app_start_timer();

	/******************************************************************************
	* app task initial
	*******************************************************************************/
	app_task_init();

	/******************************************************************************
	* run applications
	*******************************************************************************/
	return task_run();
}

/*****************************************************************************/
/* app initial function.
 */
/*****************************************************************************/

/* start software timer for application
 * used for app tasks
 */
void app_start_timer() {
	/* start timer to toggle life led */
	timer_set(AC_TASK_LIFE_ID, AC_LIFE_SYSTEM_CHECK, AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);
}

/* send first message to trigger start tasks
 * used for app tasks
 */
void app_task_init() {
	{
		ak_msg_t* msg = get_pure_msg();
		set_msg_sig(msg, AC_RF24_INIT);
		task_post(AC_TASK_RF24_ID, msg);
	}

	//{
	//	ak_msg_t* msg = get_pure_msg();
	//	set_msg_sig(msg, AC_SENSOR_INIT);
	//	task_post(AC_TASK_SENSOR_ID, msg);
	//}
}
