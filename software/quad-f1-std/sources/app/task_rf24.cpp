#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"

#include "../rf_protocols/RF24/RF24.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_rf24.h"

#define RF_CHANNEL			120
#define	RF_PAYLOAD_LEN		10

static uint8_t RF_ADDR[][7]		= {"1QUAD", "REMOTE"};

RF24 radio(1, 2);

void sys_irq_nrf24l01() {

	bool tx_ok, tx_fail, rx_ready;
	radio.whatHappened(tx_ok, tx_fail, rx_ready);

	if (rx_ready) {
		ak_msg_t* msg = get_pure_msg();
		set_msg_sig(msg, AC_RF24_IRQ_RX_READY);
		task_post(AC_TASK_RF24_ID, msg);
	}
}

void task_rf24(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_RF24_INIT:
		APP_DBG("AC_RF24_INIT\n");

		radio.begin();
		radio.maskIRQ(false, false, true);  /* enable rx interrupt */
		radio.setChannel(RF_CHANNEL);
		radio.setAutoAck(false);
		//radio.setRetries(15, 15);
		radio.setDataRate(RF24_250KBPS);;
		radio.setPALevel(RF24_PA_MAX);
		radio.setPayloadSize(RF_PAYLOAD_LEN);
		radio.openWritingPipe(RF_ADDR[1]);
		radio.openReadingPipe(1,RF_ADDR[0]);
		radio.startListening();

		radio.printDetails();

		break;

	case AC_RF24_IRQ_RX_READY: {
		APP_DBG("AC_RF24_IRQ_RX_READY\n");

		if(radio.available()){
			uint8_t data[RF_PAYLOAD_LEN];
			radio.read(&data,RF_PAYLOAD_LEN);

			APP_DBG("[%2X %2X %2X %2X %2X %2X %2X %2X %2X %2X]\n",
					data[0], data[1], data[2], data[3],data[4],
					data[5], data[6], data[7], data[8], data[9]);
		}

	}
		break;

	default:
		break;
	}
}

