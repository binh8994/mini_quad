#include "../ak/fsm.h"
#include "../ak/port.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"

#include "../common/utils.h"

#include "../platform/stm32f10x/io_cfg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_sensor.h"
#include "l3g4200d.h"

void l3g4_cfg(L3G_ConfigTypeDef*);
void l3g4_read_buffer(uint8_t*);
void l3g4_read_raw_data(int16_t*);
void l3g4_read_data(float*);

void task_sensor(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_SENSOR_INIT: {
		APP_DBG("AC_SENSOR_INIT\n");

		L3G_ConfigTypeDef L3G_InitStructure;
		L3G_InitStructure.ODR = L3G_ODR_100;
		L3G_InitStructure.Bandwidth = L3G_BW_Conf1;
		L3G_InitStructure.Power = L3G_PD_On;
		L3G_InitStructure.Axis_Enable = L3G_XYZEN;
		L3G_InitStructure.FS = L3G_FS_250;
		L3G_InitStructure.BDU = L3G_BDU_Continous;
		L3G_InitStructure.Endianess = L3G_Big_Endian;

		l3g4_cfg(&L3G_InitStructure);

//		ak_msg_t* msg = get_pure_msg();
//		set_msg_sig(msg, AC_SENSOR_CALIB);
//		task_post(AC_TASK_SENSOR_ID, msg);

	}
		break;

	case AC_SENSOR_CALIB:
		APP_DBG("AC_SENSOR_CALIB\n");

		mem_set(&global_sensor, 0, sizeof(global_sensor_t));

		for(uint32_t i=0; i<2000; i++) {
			float rpy[3];
			l3g4_read_data(rpy);

			global_sensor.roll_cal	+= rpy[0];
			global_sensor.pitch_cal	+= rpy[1];
			global_sensor.yaw_cal	+= rpy[2];
		}

		global_sensor.roll_cal	/= 2000;
		global_sensor.pitch_cal	/= 2000;
		global_sensor.yaw_cal	/= 2000;

		APP_DBG("Calib [r][p][y]\t%f\t%f\t%f\n",
				global_sensor.roll_cal,
				global_sensor.pitch_cal,
				global_sensor.yaw_cal);


		timer_set(AC_TASK_SENSOR_ID, AC_SENSOR_READ, AC_SENSOR_READ_INTERVAL, TIMER_PERIODIC);

		break;

	case AC_SENSOR_READ: {

		float rpy[3];
		l3g4_read_data(rpy);

		rpy[0] = rpy[0] - global_sensor.roll_cal;
		rpy[1] = rpy[1] - global_sensor.pitch_cal;
		rpy[2] = rpy[2] - global_sensor.yaw_cal;

		global_sensor.roll_input = global_sensor.roll_input*0.8 + ((rpy[0] / 57.14286) * 0.2);
		global_sensor.pitch_input = global_sensor.pitch_input*0.8 + ((rpy[1] / 57.14286) * 0.2);
		global_sensor.yaw_input = global_sensor.yaw_input*0.8 + ((rpy[2] / 57.14286) * 0.2);

		APP_DBG("Input [r][p][y]\t%f\t%f\t%f\n",
				global_sensor.roll_input,
				global_sensor.pitch_input,
				global_sensor.yaw_input);

	}
		break;

	default:
		break;
	}
}



/**
* @brief  Set configuration of Angular Acceleration measurement of L3G4200D
*/

void l3g4_cfg(L3G_ConfigTypeDef *L3G_Config_Struct) {
	uint8_t CRTL1 = 0x00;
	uint8_t CRTL4 = 0x00;

	CRTL1 |= (uint8_t) (L3G_Config_Struct->ODR | L3G_Config_Struct->Bandwidth |
						L3G_Config_Struct->Power | L3G_Config_Struct->Axis_Enable);

	CRTL4 |= (uint8_t) (L3G_Config_Struct->BDU | L3G_Config_Struct->Endianess | L3G_Config_Struct->FS);

	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL1, L3G_CTRL_REG1);
	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL4, L3G_CTRL_REG4);

	//uint8_t DT = 0x90;
	//i2c_byte_write(L3G_I2C_ADDRESS, &DT, L3G_CTRL_REG4);
}


void l3g4_read_buffer(uint8_t* out) {
	i2c_buffer_read(L3G_I2C_ADDRESS,  out, L3G_OUT_X_L|0x80, 6);
}

/**
* @brief   Read L3G4200D output register
* @param  out : buffer to store data
* @retval None
*/

void l3g4_read_raw_data(int16_t* out) {
	uint8_t buffer[6];
	uint8_t crtl4;
	uint8_t i = 0;

	i2c_buffer_read(L3G_I2C_ADDRESS, &crtl4, L3G_CTRL_REG4, 1);
	i2c_buffer_read(L3G_I2C_ADDRESS, buffer, L3G_OUT_X_L|0x80, 6);

	/* check in the control register4 the data alignment*/
	if(!(crtl4 & 0x40))	{
		for(i=0; i<3; i++) {
			out[i]=(int16_t)(((uint16_t)buffer[2*i+1] << 8) + buffer[2*i]);
		}
	}
	else {
		for(i=0; i<3; i++) {
			out[i]=((int16_t)((uint16_t)buffer[2*i] << 8) + buffer[2*i+1]);
		}
	}
}

void l3g4_read_data(float* out) {
	int16_t tmp[3];
	l3g4_read_raw_data(tmp);

	out[0] = (float)tmp[0] * L3G_Gyr_Coeff;
	out[1] = (float)tmp[1] * L3G_Gyr_Coeff;
	out[2] = (float)tmp[2] * L3G_Gyr_Coeff;
}

