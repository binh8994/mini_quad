#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"
#include "../ak/task.h"

extern task_t app_task_table[];

/*****************************************************************************/
/*  DECLARE: Internal Task ID
 *  Note: Task id MUST be increasing order.
 */
/*****************************************************************************/
/**
  * SYSTEM TASKS
  **************/
#define TASK_TIMER_TICK_ID				0

/**
  * APP TASKS
  **************/
#define AC_TASK_SHELL_ID				1
#define AC_TASK_LIFE_ID					2
#define	AC_TASK_RF24_ID					3
#define	AC_TASK_SENSOR_ID				4
/**
  * EOT task ID
  **************/
#define AC_TASK_EOT_ID					5

/*****************************************************************************/
/*  DECLARE: Task entry point
 */
/*****************************************************************************/
extern void task_shell(ak_msg_t*);
extern void task_life(ak_msg_t*);
extern void task_rf24(ak_msg_t*);
extern void task_sensor(ak_msg_t*);

#endif //__TASK_LIST_H__
