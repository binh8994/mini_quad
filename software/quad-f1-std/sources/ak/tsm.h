/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   12/02/2017
 * @brief:  table state machine.
 ******************************************************************************
**/
#ifndef __TSM_H__
#define __TSM_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include "ak.h"
#include "message.h"

//typedef void (*tsm_func_f)(ak_msg_t*);

//typedef uint8_t tsm_state_t;

//typedef struct {
//	uint8_t sig;
//	tsm_state_t next_state;
//	tsm_func_f tsm_func;
//} tsm_t;

//typedef tsm_t* tsm_tbl;

//#define TSM(me, init_func)  ((tsm_t*)me)->state = (state_handler)init_func
//#define TSM_TRAN(me, target)    ((tsm_t*)me)->state = (state_handler)target

//void tsm_init(tsm_t* tsm_tran, tsm_state_t state);
//void tsm_dispatch(fsm_t* me, ak_msg_t* msg);
//void tsm_tran(fsm_t* me, ak_msg_t* msg);

#ifdef __cplusplus
}
#endif

#endif //__TSM_H__
