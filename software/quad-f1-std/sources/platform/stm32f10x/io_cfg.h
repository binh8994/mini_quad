/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_Pin_13)
#define LED_LIFE_IO_PORT				(GPIOC)
#define LED_LIFE_IO_CLOCK				(RCC_APB2Periph_GPIOC)

/*****************************************************************************
 *Pin map nRF24l01
******************************************************************************/
#define NRF_CE_IO_PIN					(GPIO_Pin_3)
#define NRF_CE_IO_PORT					(GPIOA)
#define NRF_CE_IO_CLOCK					(RCC_APB2Periph_GPIOA)


#define NRF_CSN_IO_PIN					(GPIO_Pin_4)
#define NRF_CSN_IO_PORT					(GPIOA)
#define NRF_CSN_IO_CLOCK				(RCC_APB2Periph_GPIOA)

#define NRF_IRQ_IO_PIN					(GPIO_Pin_0)
#define NRF_IRQ_IO_PORT					(GPIOB)
#define NRF_IRQ_IO_CLOCK				(RCC_APB2Periph_GPIOB)

/****************************************************************************
 *Pin map CT sensor
*****************************************************************************/
#define BAT_ADC_PIN						(GPIO_Pin_0)
#define BAT_ADC_PORT					(GPIOA)
#define BAT_ADC_IO_CLOCK				(RCC_APB2Periph_GPIOA)
#define BAT_ADC_CHANNEL					(ADC_Channel_0)

/****************************************************************************
 *Define l3g4200d gyro sensor
*****************************************************************************/
#define L3G_I2C							(I2C2)
#define L3G_I2C_RCC_Periph				(RCC_APB1Periph_I2C2)
#define L3G_I2C_Port					(GPIOB)
#define L3G_I2C_SCL_Pin					(GPIO_Pin_10)
#define L3G_I2C_SDA_Pin					(GPIO_Pin_11)
#define L3G_I2C_RCC_Port				(RCC_APB2Periph_GPIOB)
#define L3G_I2C_Speed					(400000)
#define L3G_I2C_REMAP					(GPIO_Remap_I2C1)
/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
extern void nrf24l01_io_ctrl_init();
extern void nrf24l01_ce_low();
extern void nrf24l01_ce_high();
extern void nrf24l01_csn_low();
extern void nrf24l01_csn_high();

/******************************************************************************
* adc function
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
/* configure adc peripheral */
extern void io_cfg_adc1(void);

/* adc configure io */
extern void adc_batery_io_cfg();
extern uint16_t adc_batery_io_read();

/******************************************************************************
* l3g4200d function
*******************************************************************************/
void cfg_i2c2(void);
void io_cfg_i2c2(void);
void i2c_byte_write(uint8_t slAddr, uint8_t* pBuffer, uint8_t WriteAddr);
void i2c_buffer_read(uint8_t slAddr,uint8_t* pBuffer, uint8_t ReadAddr, uint8_t NumByteToRead);


uint8_t I2C_Read(uint8_t* buf, uint32_t nbuf, uint8_t SlaveAddress);
uint8_t I2C_Write(const uint8_t* buf, uint32_t nbuf,  uint8_t SlaveAddress);

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
