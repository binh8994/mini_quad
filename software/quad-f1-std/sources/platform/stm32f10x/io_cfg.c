/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"
#include "arduino/Arduino.h"

#include "../sys/sys_dbg.h"

#include "../common/utils.h"
#include "../app/app_dbg.h"

#include "../app/l3g4200d.h"

#define Timed(x) Timeout = 0xFFFF; while (x) { if (Timeout-- == 0) goto errReturn;}

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef        GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(LED_LIFE_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_LIFE_IO_PORT, &GPIO_InitStructure);
}

void led_life_on() {
	GPIO_SetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

void led_life_off() {
	GPIO_ResetBits(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN);
}

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
void nrf24l01_io_ctrl_init() {
	/* CE / CSN / IRQ */
	GPIO_InitTypeDef        GPIO_InitStructure;
	EXTI_InitTypeDef        EXTI_InitStruct;
	NVIC_InitTypeDef        NVIC_InitStruct;

	/* GPIOA, GPIOB Periph clock enable */
	RCC_APB2PeriphClockCmd(NRF_CE_IO_CLOCK, ENABLE);
	//RCC_APB2PeriphClockCmd(NRF_CSN_IO_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(NRF_IRQ_IO_CLOCK, ENABLE);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	/*CE -> PA3*/
	GPIO_InitStructure.GPIO_Pin = NRF_CE_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(NRF_CE_IO_PORT, &GPIO_InitStructure);

	/*CNS -> PA4*/
	GPIO_InitStructure.GPIO_Pin = NRF_CSN_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(NRF_CSN_IO_PORT, &GPIO_InitStructure);

	/* IRQ -> PB0 */
	GPIO_InitStructure.GPIO_Pin = NRF_IRQ_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(NRF_IRQ_IO_PORT, &GPIO_InitStructure);

	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);

	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

void nrf24l01_ce_low() {
	GPIO_ResetBits(NRF_CE_IO_PORT, NRF_CE_IO_PIN);
}

void nrf24l01_ce_high() {
	GPIO_SetBits(NRF_CE_IO_PORT, NRF_CE_IO_PIN);
}

void nrf24l01_csn_low() {
	GPIO_ResetBits(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN);
}

void nrf24l01_csn_high() {
	GPIO_SetBits(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN);
}

/******************************************************************************
* adc function
* + CT sensor
* + themistor sensor
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
void io_cfg_adc1(void) {
	ADC_InitTypeDef ADC_InitStructure;

	RCC_HSICmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

	/* Enable ADC1 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 , ENABLE);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode =DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);
}

void adc_batery_io_cfg() {
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(BAT_ADC_IO_CLOCK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = BAT_ADC_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BAT_ADC_PORT, &GPIO_InitStructure);
}

uint16_t adc_batery_io_read() {
	ADC_RegularChannelConfig(ADC1, BAT_ADC_CHANNEL, 1, ADC_SampleTime_28Cycles5);

	/* Enable ADC1 reset calibration register */
	ADC_ResetCalibration(ADC1);
	/* Check the end of ADC1 reset calibration register */
	while(ADC_GetResetCalibrationStatus(ADC1));

	/* Start ADC1 calibration */
	ADC_StartCalibration(ADC1);
	/* Check the end of ADC1 calibration */
	while(ADC_GetCalibrationStatus(ADC1));

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);

	return ADC_GetConversionValue(ADC1);
}

void io_cfg_i2c2() {
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable I2C, GPIO and AFIO clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | L3G_I2C_RCC_Port, ENABLE);

	/* Configure I2C pins: SCL and SDA */
	GPIO_InitStructure.GPIO_Pin =  L3G_I2C_SDA_Pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(L3G_I2C_Port, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = L3G_I2C_SCL_Pin;
	GPIO_Init(L3G_I2C_Port, &GPIO_InitStructure);

}

void cfg_i2c2(void) {
	I2C_InitTypeDef  I2C_InitStructure;

	I2C_DeInit(L3G_I2C);
	I2C_StructInit(&I2C_InitStructure);

	RCC_APB1PeriphClockCmd(L3G_I2C_RCC_Periph, ENABLE);

	/* I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = L3G_I2C_Speed;

	/* I2C Peripheral Disable */
	I2C_Cmd(L3G_I2C, DISABLE);
	/* Apply I2C configuration after enabling it */
	I2C_Init(L3G_I2C, &I2C_InitStructure);
	/* I2C Peripheral Enable */
	I2C_Cmd(L3G_I2C, ENABLE);

}

/**
* @brief  Writes one byte to the  L3G4200D.
* @param  slAddr : slave address L3G_I2C_ADDRESS
* @param  pBuffer : pointer to the buffer  containing the data to be written to the L3G4200D.
* @param  WriteAddr : address of the register in which the data will be written
* @retval None
*/
void i2c_byte_write(uint8_t slAddr, uint8_t* pBuffer, uint8_t WriteAddr) {

	//while(I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BUSY));

	APP_DBG("I2C_GenerateSTART\n");
	/* Send START condition */
	I2C_GenerateSTART(L3G_I2C, ENABLE);

	/* Test on EV5 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_MODE_SELECT));
	//APP_DBG("1\n");

	/* Send L3G4200D address for write */
	I2C_Send7bitAddress(L3G_I2C, slAddr, I2C_Direction_Transmitter);
	//APP_DBG("2\n");

	/* Test on EV6 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	APP_DBG("3\n");

	/* Send the L3G4200D internal address to write to */
	I2C_SendData(L3G_I2C, WriteAddr);
	//APP_DBG("4\n");

	/* Test on EV8 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
	//APP_DBG("5\n");

	/* Send the byte to be written */
	I2C_SendData(L3G_I2C, *pBuffer);
	//APP_DBG("6\n");

	/* Test on EV8 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	APP_DBG("I2C_GenerateSTOP\n");
	/* Send STOP condition */
	I2C_GenerateSTOP(L3G_I2C, ENABLE);
	while (I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_STOPF));
}

/**
* @brief  Reads a block of data from the L3G4200D.
* @param  slAddr  : slave address L3G_I2C_ADDRESS
* @param  pBuffer : pointer to the buffer that receives the data read from the L3G4200D.
* @param  ReadAddr : L3G4200D's internal address to read from.
* @param  NumByteToRead : number of bytes to read from the L3G4200D.
* @retval None
*/

void i2c_buffer_read(uint8_t slAddr,uint8_t* pBuffer, uint8_t ReadAddr, uint8_t NumByteToRead) {
	/* While the bus is busy */
	while(I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BUSY));

	/* Send START condition */
	I2C_GenerateSTART(L3G_I2C, ENABLE);

	/* Test on EV5 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_MODE_SELECT));

	/* Send L3G4200D_Magn address for write */
	I2C_Send7bitAddress(L3G_I2C, slAddr, I2C_Direction_Transmitter);

	/* Test on EV6 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	/* Clear EV6 by setting again the PE bit */
	I2C_Cmd(L3G_I2C, ENABLE);

	/* Send the L3G4200D_Magn's internal address to write to */
	I2C_SendData(L3G_I2C, ReadAddr);

	/* Test on EV8 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	/* Send STRAT condition a second time */
	I2C_GenerateSTART(L3G_I2C, ENABLE);

	/* Test on EV5 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_MODE_SELECT));

	/* Send L3G4200D address for read */
	I2C_Send7bitAddress(L3G_I2C, slAddr, I2C_Direction_Receiver);

	/* Test on EV6 and clear it */
	while(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

	/* While there is data to be read */
	while(NumByteToRead)
	{
		if(NumByteToRead == 1)
		{
			/* Disable Acknowledgement */
			I2C_AcknowledgeConfig(L3G_I2C, DISABLE);

			/* Send STOP Condition */
			I2C_GenerateSTOP(L3G_I2C, ENABLE);
		}

		/* Test on EV7 and clear it */
		if(I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED))
		{
			/* Read a byte from the L3G4200D */
			*pBuffer = I2C_ReceiveData(L3G_I2C);

			/* Point to the next location where the byte read will be saved */
			pBuffer++;

			/* Decrement the read bytes counter */
			NumByteToRead--;
		}
	}

	/* Enable Acknowledgement to be ready for another reception */
	I2C_AcknowledgeConfig(L3G_I2C, ENABLE);
}


uint8_t I2C_Read(uint8_t *buf,uint32_t nbyte, uint8_t SlaveAddress) {
	__IO uint32_t Timeout = 0;

	//    I2Cx->CR2 |= I2C_IT_ERR;  interrupts for errors

	if (!nbyte)
		return 1;

	// Wait for idle I2C interface

	Timed(I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BUSY));

	// Enable Acknowledgement, clear POS flag

	I2C_AcknowledgeConfig(L3G_I2C, ENABLE);
	I2C_NACKPositionConfig(L3G_I2C, I2C_NACKPosition_Current);

	// Intiate Start Sequence (wait for EV5

	I2C_GenerateSTART(L3G_I2C, ENABLE);
	Timed(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_MODE_SELECT));

	// Send Address

	I2C_Send7bitAddress(L3G_I2C, SlaveAddress, I2C_Direction_Receiver);

	// EV6

	Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_ADDR));

	if (nbyte == 1) {

		// Clear Ack bit

		I2C_AcknowledgeConfig(L3G_I2C, DISABLE);

		// EV6_1 -- must be atomic -- Clear ADDR, generate STOP

		__disable_irq();
		(void) L3G_I2C->SR2;
		I2C_GenerateSTOP(L3G_I2C,ENABLE);
		__enable_irq();

		// Receive data   EV7

		Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_RXNE));
		*buf++ = I2C_ReceiveData(L3G_I2C);

	}
	else if (nbyte == 2) {
		// Set POS flag

		I2C_NACKPositionConfig(L3G_I2C, I2C_NACKPosition_Next);

		// EV6_1 -- must be atomic and in this order

		__disable_irq();
		(void) L3G_I2C->SR2;                           // Clear ADDR flag
		I2C_AcknowledgeConfig(L3G_I2C, DISABLE);       // Clear Ack bit
		__enable_irq();

		// EV7_3  -- Wait for BTF, program stop, read data twice

		Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BTF));

		__disable_irq();
		I2C_GenerateSTOP(L3G_I2C,ENABLE);
		*buf++ = L3G_I2C->DR;
		__enable_irq();

		*buf++ = L3G_I2C->DR;

	}
	else {
		(void) L3G_I2C->SR2;                           // Clear ADDR flag
		while (nbyte-- != 3) {
			// EV7 -- cannot guarantee 1 transfer completion time, wait for BTF
			//        instead of RXNE

			Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BTF));
			*buf++ = I2C_ReceiveData(L3G_I2C);
		}

		Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BTF));

		// EV7_2 -- Figure 1 has an error, doesn't read N-2 !

		I2C_AcknowledgeConfig(L3G_I2C, DISABLE);           // clear ack bit

		__disable_irq();
		*buf++ = I2C_ReceiveData(L3G_I2C);             // receive byte N-2
		I2C_GenerateSTOP(L3G_I2C,ENABLE);                  // program stop
		__enable_irq();

		*buf++ = I2C_ReceiveData(L3G_I2C);             // receive byte N-1

		// wait for byte N

		Timed(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED));
		*buf++ = I2C_ReceiveData(L3G_I2C);

		nbyte = 0;

	}

	// Wait for stop

	Timed(I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_STOPF));
	return 1;

errReturn:

	// Any cleanup here
	return 0;

}

uint8_t I2C_Write(const uint8_t* buf,  uint32_t nbyte, uint8_t SlaveAddress)
{
	__IO uint32_t Timeout = 0;

	/* Enable Error IT (used in all modes: DMA, Polling and Interrupts */
	//    I2Cx->CR2 |= I2C_IT_ERR;

	if (nbyte) {
		Timed(I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BUSY));

		// Intiate Start Sequence

		I2C_GenerateSTART(L3G_I2C, ENABLE);
		Timed(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_MODE_SELECT));

		// Send Address  EV5

		I2C_Send7bitAddress(L3G_I2C, SlaveAddress, I2C_Direction_Transmitter);
		Timed(!I2C_CheckEvent(L3G_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

		// EV6

		// Write first byte EV8_1

		I2C_SendData(L3G_I2C, *buf++);

		while (--nbyte) {

			// wait on BTF

			Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BTF));
			I2C_SendData(L3G_I2C, *buf++);
		}

		Timed(!I2C_GetFlagStatus(L3G_I2C, I2C_FLAG_BTF));
		I2C_GenerateSTOP(L3G_I2C, ENABLE);
		Timed(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));
	}
	return 1;
errReturn:
	return 0;
}
