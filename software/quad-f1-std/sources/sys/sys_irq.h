#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern void sys_irq_nrf24l01();
extern void sys_irq_shell();

#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
