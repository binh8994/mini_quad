/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "MPU9250SPI.h"
#include "pid.h"
#include "ppm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct t_axis {
	float x;
	float y;
	float z;
} axis_t;
typedef struct t_angles {
	float roll;
	float pitch;
	float yaw;
} angles_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define RAD_TO_DEG 			(57.295779513082320876798154814105)

#define GYRO_PART			(0.995f)
#define ACCEL_PART			(0.005f)

#define START_WITH_RECV		(1)
#define DEBUG_SS_EN			(0)
#define DEBUG_RECV_EN		(0)
#define DEBUG_MOTOR_EN		(0)

//===================[P]=======[I]=======[D]====//
//#define ROLL_PID	 5.0,	   0.0,	    1.00    // Rung biên độ nhỏ lien tuc, đáp ứng chua đủ
//#define ROLL_PID	 5.0,	   0.0,	    0.0     // Dao dong qua lai nhieu, dap ung du
//#define ROLL_PID	 5.0,	   0.0,	    0.01    // Dao dong qua lai nhieu, dap ung du
//#define ROLL_PID	 5.0,	   0.0,	    0.05    // Dao dong qua lai nhieu, dap ung du, nghieng 1 ben
//#define ROLL_PID	 5.0,	   0.0,	    0.2     // Ga nhỏ dao dong nhieu, ga lớn ko du dap ung, nghieng 1 bên
//#define ROLL_PID	 6.0,	   0.0,	    1.00    // Rung bien do nho lien tuc
//#define ROLL_PID	 6.0,	   0.0,	    0.1     // Dao động qua lai nhieu
//#define ROLL_PID	 6.0,	   0.0,	    0.05    // Dao dong qua lai nhieu
//#define ROLL_PID	 6.0,	   0.0,	    1.5		// Rung bien do nho lien tuc, lech 1 ben, khong dap ung
//#define ROLL_PID	 6.0,	   0.0,	    2.5     // Rung bien do nho lien tục, đáp ứng khong ổn định, lệch 1 bên
//#define ROLL_PID	 5.5,	   0.0,	    0.1		// Dao dong qua lai nhiều
//#define ROLL_PID	 5.5,	   0.1,	    0.1		// Dao dong qua lai nhiều
//#define ROLL_PID	 5.5,	   0.3,	    0.1		// Dao dong qua lai nhiều
//#define ROLL_PID	 5.5,	   1.0,	    0.1		// Dao dong qua lai nhiều
//#define ROLL_PID	 5.5,	   2.0,	    0.1		// Dao dong qua lai nhiều
//#define ROLL_PID	 4.0,	   0.0,	    0.0		// Dao dong qua lai nhiều, lech 1 ben
//#define ROLL_PID	 4.0,	   0.0,	    0.05	// Dao dong qua lai nhiều
//#define ROLL_PID	 4.0,	   0.0,	    0.15	// Lech 1 ben
//#define ROLL_PID	 4.0,	   0.5,	    0.1		// Dao dong qua lai it hon
//#define ROLL_PID	 4.0,	   1.0,	    0.1		// Dao dong qua lai it hon
//#define ROLL_PID	 4.0,	   1.5,	    0.1		// Dao dong qua lai it hon
//#define ROLL_PID	 4.0,	   2.0,	    0.1		// M1, M2 chay truoc, I qua lon
//#define ROLL_PID	 4.0,	   1.5,	    1.0		// Rung bien do nho lien tuc
//#define ROLL_PID	 4.0,	   1.5,	    0.05    // Dao dong qua lai it
//#define ROLL_PID	 4.0,	   1.5,	    0.5	    // Dao dong qua lai it
// Sua input PID calulate = inPut
//#define ROLL_PID	 4.0,	   1.5,	    0.5		// Dao dong qua lai it
//#define ROLL_PID	 6.0,	   0.0,	    0.1		// Dao dong qua lai it
//#define ROLL_PID	 6.0,	   0.0,	    0.5		// Dao dong qua lai it
//#define ROLL_PID	 6.0,	   0.0,	    1.5		// Dao dong qua lai nhieu
//#define ROLL_PID	 6.0,	   0.0,	    2.5		// Dao dong qua lai nhieu
//#define ROLL_PID	 6.0,	   1.0,	    0.5		// Dao dong qua lai it

#define ROLL_PID	 6.0,	   1.0,	    0.5
#define PITCH_PID	 0.0,	   0.0,	    0.0
#define YAW_PID		 0.0,	   0.0,	    0.0
//#define YAW_PID		12.0,	0.0,	1.0

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
float dT;
uint32_t lastUS = 0, nowUS;

axis_t accelRead, gyroRead;
axis_t accelOffset = { 0, 0, 0 }, gyroOffset = { 0, 0, 0 };
angles_t accelAngles;
angles_t cFilterAngles = { 0, 0, 0 };

float outputRoll, outputPitch, outputYaw;
float setRoll = 0, setPitch = 0, setYaw = 0, setThr = 0, setOffset = 0;
uint32_t swOffset;

int16_t motorValue1, motorValue2, motorValue3, motorValue4;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void blinkRedLed(uint8_t time);
void blinkGreenLed(uint8_t time);
void csControl(bool state);
void errCallback(void);
int16_t setMotor(uint8_t num, int16_t value);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
MPU9250SPI mpu(csControl, HAL_Delay, errCallback);
PPM ppm(8);
PID pidRoll(ROLL_PID), pidPitch(PITCH_PID), pidYaw(YAW_PID);
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_SPI3_Init();
	MX_USART1_UART_Init();
	MX_TIM2_Init();
	MX_ADC1_Init();
	MX_TIM3_Init();
	MX_TIM5_Init();
	/* USER CODE BEGIN 2 */

	printf("\r\n[MINIQUAD STARTED]\n");
	HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LED_ON);
	HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_ON);
	HAL_Delay(1000);
	HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LED_OFF);
	HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_OFF);

	printf("Starting Timerbase\n");
	HAL_TIM_Base_Start(&usTim);

	printf("Starting PPM decoder\n");
	ppm.installDriver(&ppmTim, TIM_CHANNEL_1);
	ppm.start();

	printf("Starting PWM\n");
	HAL_TIM_PWM_Start(&pwmTim, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&pwmTim, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&pwmTim, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&pwmTim, TIM_CHANNEL_4);

	printf("Setting sensor\n");
	if (!mpu.checkConnect()) {
		printf("MPU9250 cannot detected\n");
		while (1) {
			blinkRedLed(1);
		}
	}

	printf("Waiting for calibration\n");
	HAL_Delay(100);

#if START_WITH_RECV == 1
	while (ppm.getValue(PPM::ROLL, 0, 3) < 2
			|| ppm.getValue(PPM::PITCH, 0, 3) < 2
			|| ppm.getValue(PPM::YAW, 0, 3) < 2
			|| ppm.getValue(PPM::THROTTLE, 0, 3) < 2) {
		blinkGreenLed(1);
	}
#else
	HAL_Delay(1000);
#endif

	printf("MPU9250 detected\n");
	mpu.resetAccelGyro();
	HAL_Delay(100);

	float selfTest[6] = { 0 };
	float gyroBias[3] = { 0 };
	float accelBias[3] = { 0 };

	mpu.selfTestAccelGyro(selfTest);
	//printf("Accel self test: %.01f%%, %.01f%%, %.01f%%\n", selfTest[0],
	//		selfTest[1], selfTest[2]);
	//printf("Gyro self test: %.01f%%, %.01f%%, %.01f%%\n", selfTest[3],
	//		selfTest[4], selfTest[5]);

	mpu.calibAccelGyro(gyroBias, accelBias);
	//printf("Gyro bias: %.03f, %.03f, %.03f\n", gyroBias[0], gyroBias[1],
	//		gyroBias[2]);
	//printf("Accel bias: %.03f, %.03f, %.03f\n", accelBias[0], accelBias[1],
	//		accelBias[2]);
	accelBias[2] = 1.0;
	mpu.setAccelBias(accelBias);

	mpu.configAccelGyro(AFS_2G, GFS_250DPS, 4);

#if START_WITH_RECV == 1
	printf("Waiting for start\n");
	while (ppm.getValue(PPM::THROTTLE, 0, 3) > 1) {
		blinkGreenLed(2);
	}
#endif

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

#if DEBUG_SS_EN == 1
	uint32_t lastSSTime = 0;
#endif
#if	DEBUG_RECV_EN == 1
	uint32_t lastRecvTime = 0;
#endif
#if DEBUG_MOTOR_EN == 1
	uint32_t lastMotorTime = 0;
#endif

	while (1) {
		if (HAL_GPIO_ReadPin(INT_GPIO_Port, INT_Pin)) {
//		if (mpu.checkAccelGyroAvalaible()) {
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_ON);

			mpu.readAccelGyro(&accelRead.x, &accelRead.y, &accelRead.z,
					&gyroRead.x, &gyroRead.y, &gyroRead.z);

			setRoll = ppm.getValue(PPM::ROLL, -10, 10);
			setPitch = ppm.getValue(PPM::PITCH, -10, 10);
			setYaw = ppm.getValue(PPM::YAW, -5, 5);
			setThr = ppm.getValue(PPM::THROTTLE, 0, 1000);
			swOffset = ppm.getValue(PPM::SET_OFFSET, 0, 2);

			if (swOffset == 0) {
				// TODO: calc offset from avrg samples
				/* Save offset */
				accelOffset.x = accelRead.x;
				accelOffset.y = accelRead.y;
				accelOffset.z = accelRead.z;
				gyroOffset.x = gyroRead.x;
				gyroOffset.y = gyroRead.y;
				gyroOffset.z = gyroRead.z;

				/* Set begin gyro angles */
				cFilterAngles.roll = 0;
				cFilterAngles.pitch = 0;
				cFilterAngles.yaw = 0;
			}

			/* Calc accel offset  */
			accelRead.x -= accelOffset.x;
			accelRead.y -= accelOffset.y;
			accelRead.z -= accelOffset.z;
			gyroRead.x -= gyroOffset.x;
			gyroRead.y -= gyroOffset.y;
			gyroRead.z -= gyroOffset.z;

			/* Filter accels */
			// TODO: median filter (bo loc trung vi)
			/* Calc accel angles */
			if (accelRead.z != 0) {
				accelAngles.roll =
						atan2f(accelRead.y, accelRead.z) * RAD_TO_DEG;
			} else {
				accelAngles.roll = 0;
			}
			if (accelRead.y != 0 || accelRead.z != 0) {
				accelAngles.pitch = atan2f(-accelRead.x,
						sqrtf(
								powf(accelRead.y, 2)
										+ powf(accelRead.z, 2))) * RAD_TO_DEG;
			} else {
				accelAngles.pitch = 0;
			}

			/* Calc delta time  */
			nowUS = __HAL_TIM_GET_COUNTER(&usTim);
			dT = (float) (nowUS - lastUS) / 1000000.0f;
			lastUS = nowUS;

			/* Calc complementary filter */
			cFilterAngles.roll = GYRO_PART
					* (cFilterAngles.roll + (gyroRead.x * dT))
					+ ACCEL_PART * accelAngles.roll;
			cFilterAngles.pitch = GYRO_PART
					* (cFilterAngles.pitch + (gyroRead.y * dT))
					+ ACCEL_PART * accelAngles.pitch;
			cFilterAngles.yaw = gyroRead.z * dT;

#if DEBUG_SS_EN == 1
			if (HAL_GetTick() - lastSSTime >= 20) {
				lastSSTime = HAL_GetTick();
				printf("%.01f,%.01f,%.01f,", gx, gy, gz);
				printf("%.03f,%.03f,%.03f,", ax, ay, az);
				printf("%.01f,%.01f,%.01f\n", cFilterAngles.roll, cFilterAngles.pitch, cFilterAngles.yaw);
			}
#endif

#if DEBUG_RECV_EN == 1
			if (HAL_GetTick() - lastRecvTime >= 20) {
				lastRecvTime = HAL_GetTick();
				printf("%d,%d,%d,%d,%d,%d,%d,%d,", ppm.getValue(0),
						ppm.getValue(1), ppm.getValue(2), ppm.getValue(3),
						ppm.getValue(4), ppm.getValue(5), ppm.getValue(6),
						ppm.getValue(7));
				printf("%.01f,%.01f,%.01f,%.01f\n", setRoll, setPitch, setYaw,
						setThr);
			}
#endif

			outputRoll = pidRoll.calculate(cFilterAngles.roll, setRoll, dT);
			outputPitch = pidPitch.calculate(cFilterAngles.pitch, setPitch, dT);
			outputYaw = pidYaw.calculate(cFilterAngles.yaw, setYaw, dT);

			motorValue1 = setThr + (-outputRoll / 2.0) + (+outputPitch / 2.0)
					+ (-outputYaw / 2.0);
			motorValue2 = setThr + (-outputRoll / 2.0) + (-outputPitch / 2.0)
					+ (+outputYaw / 2.0);
			motorValue3 = setThr + (+outputRoll / 2.0) + (-outputPitch / 2.0)
					+ (-outputYaw / 2.0);
			motorValue4 = setThr + (+outputRoll / 2.0) + (+outputPitch / 2.0)
					+ (+outputYaw / 2.0);

			if (setThr < 350) {
				motorValue1 = motorValue2 = motorValue3 = motorValue4 = 0;
			}

			motorValue1 = setMotor(1, motorValue1);
			motorValue2 = setMotor(2, motorValue2);
			motorValue3 = setMotor(3, motorValue3);
			motorValue4 = setMotor(4, motorValue4);

#if DEBUG_MOTOR_EN == 1
			if (HAL_GetTick() - lastMotorTime >= 20) {
				lastMotorTime = HAL_GetTick();
				printf("%.01f,%.01f,%.01f,", outputRoll, outputPitch,
						outputYaw);
				printf("%d,%d,%d,%d\n", motorValue1, motorValue2,
						motorValue3, motorValue4);
			}
#endif
		} else {
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_OFF);
		}
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 168;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */
void blinkRedLed(uint8_t time) {
	while (time--) {
		HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LED_ON);
		HAL_Delay(100);
		HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LED_OFF);
		HAL_Delay(200);
	}
	HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, LED_OFF);
	HAL_Delay(1000);
}

void blinkGreenLed(uint8_t time) {
	while (time--) {
		HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_ON);
		HAL_Delay(100);
		HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_OFF);
		HAL_Delay(200);
	}
	HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, LED_OFF);
	HAL_Delay(1000);
}

void csControl(bool state) {
	HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin,
			state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void errCallback(void) {
	while (1) {
		blinkRedLed(3);
	}
}

int16_t setMotor(uint8_t num, int16_t value) {
	/* Limit motor value */
	if (value > 999) {
		value = 999;
	} else if (value < 200) {
		value = 0;
	}

	switch (num) {
	case 1:
		pwmTim.Instance->CCR1 = value;
		break;
	case 2:
		pwmTim.Instance->CCR2 = value;
		break;
	case 3:
		pwmTim.Instance->CCR3 = value;
		break;
	case 4:
		pwmTim.Instance->CCR4 = value;
		break;
	default:
		break;
	}
	return value;
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
		ppm.interruptCallback();
	}
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
		blinkRedLed(10);
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
