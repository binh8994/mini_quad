/*
 * pid.c
 *
 *  Created on: Apr 24, 2020
 *      Author: thanhbinh89
 */
#include "pid.h"

PID::PID(float p, float i, float d) {
	this->_pGain = p;
	this->_iGain = i;
	this->_dGain = d;
	this->_pMem = 0;
	this->_iMem = 0;
	this->_dMem = 0;
	this->_preError = 0;
	this->_prePreError = 0;
	this->_preOutput = 0;
}
float PID::calculate(float input, float setpoint, float dT) {
	float error = input - setpoint;
	float out;
	this->_pMem = this->_pGain * error;
	this->_iMem = this->_iMem + this->_iGain * error * dT;
	this->_dMem = this->_dGain * (error - this->_preError) / dT;
	out = this->_preOutput + this->_pMem + this->_iMem + this->_dMem;
	this->_preError = error;
	//	this->_preOutput = out;
	return out;
}
