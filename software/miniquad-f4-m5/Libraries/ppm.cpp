/*
 * ppm.c
 *
 *  Created on: Apr 25, 2020
 *      Author: thanhbinh89
 */
#include "ppm.h"

#include <string.h>

#define PPM_MIN_TIME	(1000)
#define PPM_MAX_TIME	(2000)
#define PPM_BLANK_TIME	(PPM_MAX_TIME + 100)

PPM::PPM(uint32_t maxChannel) {
	_tim = NULL;
	this->_timChannel = 0;
	this->_ic2LastValue = 0;
	this->_channel = 0;
	this->_maxChannel = maxChannel;
}

void PPM::installDriver(TIM_HandleTypeDef *tim, uint32_t timChannel) {
	this->_tim = tim;
	this->_timChannel = timChannel;
}

void PPM::start() {
	bzero((void*) _values, sizeof((void*) _values));
	HAL_TIM_IC_Start_IT(this->_tim, this->_timChannel);
}

uint16_t PPM::getValue(uint32_t channel) {
	return (uint16_t) this->_values[channel];
}

int32_t PPM::getValue(uint32_t channel, int32_t min, int32_t max) {
	return (int32_t) map((this->_values[channel]), PPM_MIN_TIME, PPM_MAX_TIME,
			min, max);
}

void PPM::interruptCallback() {
	uint32_t ic2Value;
	uint32_t ic2DiffValue = 0;
	ic2Value = HAL_TIM_ReadCapturedValue(this->_tim, this->_timChannel);
	if (ic2Value > this->_ic2LastValue) {
		ic2DiffValue = ic2Value - this->_ic2LastValue;
	} else if (ic2Value < this->_ic2LastValue) {
		ic2DiffValue = 0xFFFF - this->_ic2LastValue + ic2Value + 1;
	}
	this->_ic2LastValue = ic2Value;

	if (ic2DiffValue > PPM_BLANK_TIME) {
		this->_channel = 0;
	} else {
		if (this->_channel < this->_maxChannel) {
			this->_values[this->_channel] = ic2DiffValue;
		}
		this->_channel++;
	}
}

int32_t PPM::map(int32_t value, int32_t input_min, int32_t input_max,
		int32_t output_min, int32_t output_max) {
	int32_t v1 = (value - input_min) * 1000 / (input_max - input_min);
	int32_t v2 = v1 * (output_max - output_min) / 1000;
	return v2 + output_min;
}
