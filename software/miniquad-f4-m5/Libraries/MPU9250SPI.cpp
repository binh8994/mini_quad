#include "MPU9250SPI.h"

#include <string.h>
#include "math.h"
#include "spi.h"

MPU9250SPI::MPU9250SPI(void (*csFunc)(bool), void (*delayMsFunc)(uint32_t),
		void (*errFunc)(void)) {
	_csFunc = csFunc;
	_delayMsFunc = delayMsFunc;
	_errFunc = errFunc;
}

bool MPU9250SPI::checkConnect() {
	uint8_t who = readRegister( WHO_AM_I_MPU9250);
	/* 0x71 (MPU9250) */
	/* 0x70 (MPU6500) */
	if (0x70 != who) {
		return false;
	}
	return true;
}

void MPU9250SPI::enableI2CMaster() {
	writeRegister( USER_CTRL, 0x30);          // Enable I2C Master mode
	writeRegister( I2C_MST_CTRL, 0x1D); // I2C configuration STOP after each transaction, master I2C bus at 400 KHz
	writeRegister( I2C_MST_DELAY_CTRL, 0x81); // Use blocking data retreival and enable delay for mag sample rate mismatch
	writeRegister( I2C_SLV4_CTRL, 0x01); // Delay mag data retrieval to once every other accel/gyro data sample
}

void MPU9250SPI::resetAccelGyro() {
	writeRegister( PWR_MGMT_1, 0x80, false);
}

void MPU9250SPI::configAccelGyro(uint8_t aScale, uint8_t gScale,
		uint8_t sampleRateDiv) {
	switch (gScale) {
	case GFS_250DPS :
		_gRes = 250.0 / 32768.0;
		break;
	case GFS_500DPS :
		_gRes = 500.0 / 32768.0;
		break;
	case GFS_1000DPS :
		_gRes = 1000.0 / 32768.0;
		break;
	case GFS_2000DPS :
		_gRes = 2000.0 / 32768.0;
		break;
	default:
		_gRes = 250.0 / 32768.0;
		;
	}

	switch (aScale) {
	case AFS_2G :
		_aRes = 2.0f / 32768.0f;
		break;
	case AFS_4G :
		_aRes = 4.0f / 32768.0f;
		break;
	case AFS_8G :
		_aRes = 8.0f / 32768.0f;
		break;
	case AFS_16G :
		_aRes = 16.0f / 32768.0f;
		break;
	default:
		_aRes = 2.0f / 32768.0f;
		break;
	}

	writeRegister( PWR_MGMT_1, 0x00);
	_delayMsFunc(100);
	writeRegister( PWR_MGMT_1, 0x01); // Auto select clock source to be PLL gyroscope reference if ready else
	_delayMsFunc(200);

	// Configure Gyro and Thermometer
	// Disable FSYNC and set thermometer and gyro bandwidth to 41Hz (gyro) and 42Hz (temperature), respectively;
	// minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
	// be higher than 1 / 0.0059 = 170 Hz
	// DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
	// With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
	writeRegister( CONFIG, 0x03);
	// Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
	writeRegister( SMPLRT_DIV, sampleRateDiv); // Use a 200 Hz rate; a rate consistent with the filter update rate
	// determined inset in CONFIG above

	// Set gyroscope full scale range
	writeRegister( GYRO_CONFIG, gScale << 3);
	// Set accelerometer full-scale range configuration
	writeRegister( ACCEL_CONFIG, aScale << 3);
	// Set accelerometer sample rate configuration
	// Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
	writeRegister( ACCEL_CONFIG2, 0x03); // Write new ACCEL_CONFIG2 register value

	// The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
	// but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

	// Configure Interrupts and Bypass Enable
	// Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared
	// clear on any read operation
	writeRegister( INT_PIN_CFG, 0x30);
	// Enable raw sensor data ready
	writeRegister( INT_ENABLE, 0x01);
	_delayMsFunc(100);

//	enableI2CMaster();
}

// Function which accumulates gyro and accelerometer data after device initialization.
// It calculates the average of the at-rest readings
void MPU9250SPI::calibAccelGyro(float *gBias, float *aBias) {
	uint8_t data[12];
	uint16_t packet_count;
	int32_t gyro_bias[3] = { 0, 0, 0 }, accel_bias[3] = { 0, 0, 0 };

	// reset device
	resetAccelGyro();
	_delayMsFunc(100);

	// get stable time source; Auto select clock source to be PLL gyroscope reference if ready
	// else use the internal oscillator, bits 2:0 = 001
	writeRegister( PWR_MGMT_1, 0x01);
	writeRegister( PWR_MGMT_2, 0x00);
	_delayMsFunc(200);

	// Configure device for bias calculation
	writeRegister( INT_ENABLE, 0x00);   // Disable all interrupts
	writeRegister( FIFO_EN, 0x00);      // Disable FIFO
	writeRegister( PWR_MGMT_1, 0x00);   // Turn on internal clock source
	writeRegister( I2C_MST_CTRL, 0x00); // Disable I2C master
	writeRegister( USER_CTRL, 0x10);    // Disable FIFO and I2C master modes
	writeRegister( USER_CTRL, 0x1C, false);    // Reset FIFO and DMP
	_delayMsFunc(15);

	// Configure MPU6050 gyro and accelerometer for bias calculation
	writeRegister( CONFIG, 0x01);      // Set low-pass filter to 188 Hz
//	writeRegister( CONFIG, 0x03);
	writeRegister( SMPLRT_DIV, 0x00);
//	writeRegister( SMPLRT_DIV, 0x04);  // Set sample rate to 1 kHz
	writeRegister( GYRO_CONFIG, 0x00); // Set gyro full-scale to 250 degrees per second, maximum sensitivity
	writeRegister( ACCEL_CONFIG, 0x00); // Set accelerometer full-scale to 2 g, maximum sensitivity

//	writeRegister( ACCEL_CONFIG2, 0x03);

	const int32_t gyroSenMax = 131;
	const int32_t accelSenMax = 16384;

	// Configure FIFO to capture accelerometer and gyro data for bias calculation
	writeRegister( USER_CTRL, 0x50);   // Enable FIFO
	writeRegister( FIFO_EN, 0x78); // Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
	_delayMsFunc(40); // accumulate 40 samples in 40 milliseconds = 480 bytes

	// At end of sample accumulation, turn off FIFO sensor read
	writeRegister( FIFO_EN, 0x00);
	readRegisters( FIFO_COUNTH, 2, &data[0]); // read FIFO sample count
	packet_count = (((uint16_t) data[0] << 8) | ((uint16_t) data[1])) / 12; // How many sets of full gyro and accelerometer data for averaging

	for (int i = 0; i < packet_count; i++) {
		int16_t accel_temp[3] = { 0, 0, 0 }, gyro_temp[3] = { 0, 0, 0 };
		readRegisters( FIFO_R_W, 12, &data[0]);
		accel_temp[0] = (int16_t) (((int16_t) data[0] << 8)
				| ((int16_t) data[1]));
		accel_temp[1] = (int16_t) (((int16_t) data[2] << 8)
				| ((int16_t) data[3]));
		accel_temp[2] = (int16_t) (((int16_t) data[4] << 8)
				| ((int16_t) data[5]));
		gyro_temp[0] =
				(int16_t) (((int16_t) data[6] << 8) | ((int16_t) data[7]));
		gyro_temp[1] =
				(int16_t) (((int16_t) data[8] << 8) | ((int16_t) data[9]));
		gyro_temp[2] = (int16_t) (((int16_t) data[10] << 8)
				| ((int16_t) data[11]));

		accel_bias[0] += (int32_t) accel_temp[0];
		accel_bias[1] += (int32_t) accel_temp[1];
		accel_bias[2] += (int32_t) accel_temp[2];
		gyro_bias[0] += (int32_t) gyro_temp[0];
		gyro_bias[1] += (int32_t) gyro_temp[1];
		gyro_bias[2] += (int32_t) gyro_temp[2];

	}
	accel_bias[0] /= (int32_t) packet_count;
	accel_bias[1] /= (int32_t) packet_count;
	accel_bias[2] /= (int32_t) packet_count;
	gyro_bias[0] /= (int32_t) packet_count;
	gyro_bias[1] /= (int32_t) packet_count;
	gyro_bias[2] /= (int32_t) packet_count;

	if (accel_bias[2] > 0L) {
		accel_bias[2] -= accelSenMax; // Remove gravity from the z-axis accelerometer bias calculation
	} else {
		accel_bias[2] += accelSenMax;
	}

	// Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
	data[0] = (-gyro_bias[0] / 4 >> 8) & 0xFF; // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
	data[1] = (-gyro_bias[0] / 4) & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
	data[2] = (-gyro_bias[1] / 4 >> 8) & 0xFF;
	data[3] = (-gyro_bias[1] / 4) & 0xFF;
	data[4] = (-gyro_bias[2] / 4 >> 8) & 0xFF;
	data[5] = (-gyro_bias[2] / 4) & 0xFF;

	// Push gyro biases to hardware registers
	//	writeRegister( XG_OFFSET_H, data[0]);
	//	writeRegister( XG_OFFSET_L, data[1]);
	//	writeRegister( YG_OFFSET_H, data[2]);
	//	writeRegister( YG_OFFSET_L, data[3]);
	//	writeRegister( ZG_OFFSET_H, data[4]);
	//	writeRegister( ZG_OFFSET_L, data[5]);

	// Output scaled gyro biases for display in the main program
	_gBias[0] = gBias[0] = (float) gyro_bias[0] / (float) gyroSenMax;
	_gBias[1] = gBias[1] = (float) gyro_bias[1] / (float) gyroSenMax;
	_gBias[2] = gBias[2] = (float) gyro_bias[2] / (float) gyroSenMax;

	// Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
	// factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
	// non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
	// compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
	// the accelerometer biases calculated above must be divided by 8.

	int32_t accel_bias_reg[3] = { 0, 0, 0 }; // A place to hold the factory accelerometer trim biases
	readRegisters( XA_OFFSET_H, 2, &data[0]); // Read factory accelerometer trim values
	accel_bias_reg[0] = (int32_t) (((int16_t) data[0] << 8)
			| ((int16_t) data[1]));
	readRegisters( YA_OFFSET_H, 2, &data[0]);
	accel_bias_reg[1] = (int32_t) (((int16_t) data[0] << 8)
			| ((int16_t) data[1]));
	readRegisters( ZA_OFFSET_H, 2, &data[0]);
	accel_bias_reg[2] = (int32_t) (((int16_t) data[0] << 8)
			| ((int16_t) data[1]));

	uint32_t mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
	uint8_t mask_bit[3] = { 0, 0, 0 }; // Define array to hold mask bit for each accelerometer bias axis

	for (int i = 0; i < 3; i++) {
		if ((accel_bias_reg[i] & mask))
			mask_bit[i] = 0x01; // If temperature compensation bit is set, record that fact in mask_bit
	}

	// Construct total accelerometer bias, including calculated average accelerometer bias from above
	accel_bias_reg[0] -= (accel_bias[0] / 8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
	accel_bias_reg[1] -= (accel_bias[1] / 8);
	accel_bias_reg[2] -= (accel_bias[2] / 8);

	data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
	data[1] = (accel_bias_reg[0]) & 0xFF;
	data[1] = data[1] | mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
	data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
	data[3] = (accel_bias_reg[1]) & 0xFF;
	data[3] = data[3] | mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
	data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
	data[5] = (accel_bias_reg[2]) & 0xFF;
	data[5] = data[5] | mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

	// Apparently this is not working for the acceleration biases in the MPU-9250
	// Are we handling the temperature correction bit properly?
	// Push accelerometer biases to hardware registers
	//  writeRegister( XA_OFFSET_H, data[0]);
	//  writeRegister( XA_OFFSET_L, data[1]);
	//  writeRegister( YA_OFFSET_H, data[2]);
	//  writeRegister( YA_OFFSET_L, data[3]);
	//  writeRegister( ZA_OFFSET_H, data[4]);
	//  writeRegister( ZA_OFFSET_L, data[5]);

	// Output scaled accelerometer biases for display in the main program
	_aBias[0] = aBias[0] = (float) accel_bias[0] / (float) accelSenMax;
	_aBias[1] = aBias[1] = (float) accel_bias[1] / (float) accelSenMax;
	_aBias[2] = aBias[2] = (float) accel_bias[2] / (float) accelSenMax;
}

void MPU9250SPI::setAccelBias(float *aBias) {
	memcpy(_aBias, aBias, sizeof(_aBias));
}

void MPU9250SPI::setGyroBias(float *gBias) {
	memcpy(_gBias, gBias, sizeof(_gBias));
}

bool MPU9250SPI::checkAccelGyroAvalaible() {
	return (readRegister( INT_STATUS) & 0x01) ? true : false;
}

void MPU9250SPI::readAccelGyro(float *ax, float *ay, float *az, float *gx,
		float *gy, float *gz) {
	uint8_t rawData[14];
	int16_t data[7];
	readRegisters( ACCEL_XOUT_H, 14, &rawData[0]);
	data[0] = ((int16_t) rawData[0] << 8) | ((int16_t) rawData[1]);
	data[1] = ((int16_t) rawData[2] << 8) | ((int16_t) rawData[3]);
	data[2] = ((int16_t) rawData[4] << 8) | ((int16_t) rawData[5]);
	data[3] = ((int16_t) rawData[6] << 8) | ((int16_t) rawData[7]);
	data[4] = ((int16_t) rawData[8] << 8) | ((int16_t) rawData[9]);
	data[5] = ((int16_t) rawData[10] << 8) | ((int16_t) rawData[11]);
	data[6] = ((int16_t) rawData[12] << 8) | ((int16_t) rawData[13]);

	*ax = (float) data[0] * _aRes - _aBias[0];
	*ay = (float) data[1] * _aRes - _aBias[1];
	*az = (float) data[2] * _aRes - _aBias[2];
	*gx = (float) data[4] * _gRes - _gBias[0];
	*gy = (float) data[5] * _gRes - _gBias[1];
	*gz = (float) data[6] * _gRes - _gBias[2];
}

// Accelerometer and gyroscope self test; check calibration wrt factory settings
// Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
void MPU9250SPI::selfTestAccelGyro(float *result) {
	uint8_t rawData[6];
	uint8_t selfTest[6];
	int32_t gAvg[3] = { 0, 0, 0 }, aAvg[3] = { 0, 0, 0 };
	int32_t aSTAvg[3] = { 0, 0, 0 }, gSTAvg[3] = { 0, 0, 0 };
	float factoryTrim[6];
	uint8_t FS = 0;

	resetAccelGyro();
	_delayMsFunc(100);
	writeRegister( SMPLRT_DIV, 0x00);    		// Set gyro sample rate to 1 kHz
	writeRegister( CONFIG, 0x02); // Set gyro sample rate to 1 kHz and DLPF to 92 Hz
	writeRegister( GYRO_CONFIG, 1 << FS); // Set full scale range for the gyro to 250 dps
	writeRegister( ACCEL_CONFIG2, 0x02); // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
	writeRegister( ACCEL_CONFIG, 1 << FS); // Set full scale range for the accelerometer to 2 g

	for (int i = 0; i < 200; i++) {
		readRegisters( ACCEL_XOUT_H, 6, &rawData[0]);
		aAvg[0] += (int16_t) (((int16_t) rawData[0] << 8)
				| ((int16_t) rawData[1]));
		aAvg[1] += (int16_t) (((int16_t) rawData[2] << 8)
				| ((int16_t) rawData[3]));
		aAvg[2] += (int16_t) (((int16_t) rawData[4] << 8)
				| ((int16_t) rawData[5]));

		readRegisters( GYRO_XOUT_H, 6, &rawData[0]);
		gAvg[0] += (int16_t) (((int16_t) rawData[0] << 8)
				| ((int16_t) rawData[1]));
		gAvg[1] += (int16_t) (((int16_t) rawData[2] << 8)
				| ((int16_t) rawData[3]));
		gAvg[2] += (int16_t) (((int16_t) rawData[4] << 8)
				| ((int16_t) rawData[5]));
	}

	for (int i = 0; i < 3; i++) { // Get average of 200 values and store as average current readings
		aAvg[i] /= 200;
		gAvg[i] /= 200;
	}

	// Configure the accelerometer for self-test
	writeRegister( ACCEL_CONFIG, 0xE0); // Enable self test on all three axes and set accelerometer range to +/- 2 g
	writeRegister( GYRO_CONFIG, 0xE0); // Enable self test on all three axes and set gyro range to +/- 250 degrees/s
	_delayMsFunc(25);

	for (int ii = 0; ii < 200; ii++) {
		readRegisters( ACCEL_XOUT_H, 6, &rawData[0]);
		aSTAvg[0] += (int16_t) (((int16_t) rawData[0] << 8)
				| ((int16_t) rawData[1]));
		aSTAvg[1] += (int16_t) (((int16_t) rawData[2] << 8)
				| ((int16_t) rawData[3]));
		aSTAvg[2] += (int16_t) (((int16_t) rawData[4] << 8)
				| ((int16_t) rawData[5]));

		readRegisters( GYRO_XOUT_H, 6, &rawData[0]);
		gSTAvg[0] += (int16_t) (((int16_t) rawData[0] << 8)
				| ((int16_t) rawData[1]));
		gSTAvg[1] += (int16_t) (((int16_t) rawData[2] << 8)
				| ((int16_t) rawData[3]));
		gSTAvg[2] += (int16_t) (((int16_t) rawData[4] << 8)
				| ((int16_t) rawData[5]));
	}

	for (int i = 0; i < 3; i++) {
		aSTAvg[i] /= 200;
		gSTAvg[i] /= 200;
	}

	// Configure the gyro and accelerometer for normal operation
	writeRegister( ACCEL_CONFIG, 0x00);
	writeRegister( GYRO_CONFIG, 0x00);
	_delayMsFunc(25);  // Delay a while to let the device stabilize

	// Retrieve accelerometer and gyro factory Self-Test Code from USR_Reg
	selfTest[0] = readRegister( SELF_TEST_X_ACCEL); // X-axis accel self-test results
	selfTest[1] = readRegister( SELF_TEST_Y_ACCEL); // Y-axis accel self-test results
	selfTest[2] = readRegister( SELF_TEST_Z_ACCEL); // Z-axis accel self-test results
	selfTest[3] = readRegister( SELF_TEST_X_GYRO); // X-axis gyro self-test results
	selfTest[4] = readRegister( SELF_TEST_Y_GYRO); // Y-axis gyro self-test results
	selfTest[5] = readRegister( SELF_TEST_Z_GYRO); // Z-axis gyro self-test results

	// Retrieve factory self-test value from self-test code reads
	factoryTrim[0] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[0] - 1.0))); // FT[Xa] factory trim calculation
	factoryTrim[1] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[1] - 1.0))); // FT[Ya] factory trim calculation
	factoryTrim[2] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[2] - 1.0))); // FT[Za] factory trim calculation
	factoryTrim[3] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[3] - 1.0))); // FT[Xg] factory trim calculation
	factoryTrim[4] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[4] - 1.0))); // FT[Yg] factory trim calculation
	factoryTrim[5] = (float) (2620 / 1 << FS)
			* (pow(1.01, ((float) selfTest[5] - 1.0))); // FT[Zg] factory trim calculation

	// Report results as a ratio of (STR - FT)/FT; the change from Factory Trim of the Self-Test Response
	for (int i = 0; i < 3; i++) {
		result[i] = 100.0f * ((float) (aSTAvg[i] - aAvg[i])) / factoryTrim[i]
				- 100.0f;
		result[i + 3] = 100.0f * ((float) (gSTAvg[i] - gAvg[i]))
				/ factoryTrim[i + 3] - 100.0f;
	}
}

bool MPU9250SPI::checkMagConnect() {
	if (0x48 != readAK8963Register( WHO_AM_I_AK8963, 10)) {
		return false;
	}
	return true;
}

void MPU9250SPI::readMagFactoryCalib(float *magCalibration) {
	uint8_t rawData[3];
	writeAK8963Register(AK8963_CNTL2, 0x01, 500);
	writeAK8963Register(AK8963_CNTL, 0x0F, 10);
	readAK8963Registers(AK8963_ASAX, 3, rawData, 10);
	_magCalib[0] = magCalibration[0] = (float) (rawData[0] + 128) / 256.0f;
	_magCalib[1] = magCalibration[1] = (float) (rawData[1] + 128) / 256.0f;
	_magCalib[2] = magCalibration[2] = (float) (rawData[2] + 128) / 256.0f;
	writeAK8963Register(AK8963_CNTL, 0x00, 10);
}

void MPU9250SPI::configMag(uint8_t mScale, uint8_t mMode) {
	switch (mScale) {
	case MFS_14BITS :
		_mRes = 10.0 * 4912.0 / 8190.0;
		break;
	case MFS_16BITS :
		_mRes = 10.0 * 4912.0 / 32760.0;
		break;
	}
	writeAK8963Register(AK8963_CNTL2, 0x01, 500);
	writeAK8963Register(AK8963_CNTL, mScale << 4 | mMode, 10);
}

void MPU9250SPI::calManualMag(uint8_t mMode, float *magBias, float *magScale) {
	uint16_t ii = 0, sample_count = 0;
	int32_t mag_bias[3] = { 0, 0, 0 }, mag_scale[3] = { 0, 0, 0 };
	int16_t mag_max[3] = { -32767, -32767, -32767 }, mag_min[3] = { 32767,
			32767, 32767 };
	int16_t mag_temp[3] = { 0, 0, 0 };
	uint8_t rawData[7];

	// shoot for ~fifteen seconds of mag data
	if (mMode == M_8Hz) {
		sample_count = 128; // at 8 Hz ODR, new mag data is available every 125 ms
	} else if (mMode == M_100Hz) {
		sample_count = 1500; // at 100 Hz ODR, new mag data is available every 10 ms
	}

	for (ii = 0; ii < sample_count; ii++) {
		readAK8963Registers(AK8963_XOUT_L, 7, rawData, 10);
		uint8_t st2 = rawData[6];
		if (!(st2 & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
			mag_temp[0] = ((int16_t) rawData[1] << 8) | ((int16_t) rawData[0]); // Turn the MSB and LSB into a signed 16-bit value
			mag_temp[1] = ((int16_t) rawData[3] << 8) | ((int16_t) rawData[2]); // Data stored as little Endian
			mag_temp[2] = ((int16_t) rawData[5] << 8) | ((int16_t) rawData[4]);
		}

		for (int jj = 0; jj < 3; jj++) {
			if (mag_temp[jj] > mag_max[jj])
				mag_max[jj] = mag_temp[jj];
			if (mag_temp[jj] < mag_min[jj])
				mag_min[jj] = mag_temp[jj];
		}
		if (mMode == M_8Hz) {
			_delayMsFunc(135); // at 8 Hz ODR, new mag data is available every 125 ms
		} else if (mMode == M_100Hz) {
			_delayMsFunc(12); // at 100 Hz ODR, new mag data is available every 10 ms
		}
	}

	// Get hard iron correction
	mag_bias[0] = (mag_max[0] + mag_min[0]) / 2; // get average x mag bias in counts
	mag_bias[1] = (mag_max[1] + mag_min[1]) / 2; // get average y mag bias in counts
	mag_bias[2] = (mag_max[2] + mag_min[2]) / 2; // get average z mag bias in counts
	_mBias[0] = magBias[0] = (float) mag_bias[0] * _mRes * _magCalib[0];
	_mBias[1] = magBias[1] = (float) mag_bias[1] * _mRes * _magCalib[1];
	_mBias[2] = magBias[2] = (float) mag_bias[2] * _mRes * _magCalib[2];

	// Get soft iron correction estimate
	mag_scale[0] = (mag_max[0] - mag_min[0]) / 2;
	mag_scale[1] = (mag_max[1] - mag_min[1]) / 2;
	mag_scale[2] = (mag_max[2] - mag_min[2]) / 2;

	float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
	avg_rad /= 3.0;
	_mScale[0] = magScale[0] = avg_rad / ((float) mag_scale[0]);
	_mScale[1] = magScale[1] = avg_rad / ((float) mag_scale[1]);
	_mScale[2] = magScale[2] = avg_rad / ((float) mag_scale[2]);

}

void MPU9250SPI::setMagBias(float *mBias) {
	memcpy(_mBias, mBias, sizeof(_mBias));
}

void MPU9250SPI::setMagScale(float *mScale) {
	memcpy(_mScale, mScale, sizeof(_mScale));
}

bool MPU9250SPI::checkMagAvalaible() {
	return (readAK8963Register(AK8963_ST1)) ? true : false;
}

void MPU9250SPI::readMag(float *mx, float *my, float *mz) {
	uint8_t rawData[7];
	int16_t data[3];
	readAK8963Registers(AK8963_XOUT_L, 7, rawData);
	uint8_t st2 = rawData[6];
	if (!(st2 & (uint8_t) 0x08)) {
		data[0] = ((int16_t) rawData[1] << 8) | ((int16_t) rawData[0]);
		data[1] = ((int16_t) rawData[3] << 8) | ((int16_t) rawData[2]);
		data[2] = ((int16_t) rawData[5] << 8) | ((int16_t) rawData[4]);
		(*mx) = (float) data[0] * _mRes * _magCalib[0] - _mBias[0];
		(*my) = (float) data[1] * _mRes * _magCalib[1] - _mBias[1];
		(*mz) = (float) data[2] * _mRes * _magCalib[2] - _mBias[2];
		(*mx) *= _mScale[0];
		(*my) *= _mScale[1];
		(*mz) *= _mScale[2];
	}
}

void MPU9250SPI::setReadMagContinuous() {
	writeRegister( I2C_SLV0_ADDR, 0x80 | AK8963_ADDRESS, false);
	writeRegister( I2C_SLV0_REG, AK8963_XOUT_L, false);
	writeRegister( I2C_SLV0_CTRL, (uint8_t) 0x87, false);
	_delayMsFunc(10);
}

void MPU9250SPI::readMagContinuous(float *mx, float *my, float *mz) {
	uint8_t rawData[7];
	int16_t data[3];
	readRegisters(EXT_SENS_DATA_00, 7, rawData);
	uint8_t st2 = rawData[6];
	if (!(st2 & (uint8_t) 0x08)) {
		data[0] = ((int16_t) rawData[1] << 8) | ((int16_t) rawData[0]);
		data[1] = ((int16_t) rawData[3] << 8) | ((int16_t) rawData[2]);
		data[2] = ((int16_t) rawData[5] << 8) | ((int16_t) rawData[4]);
		(*mx) = (float) data[0] * _mRes * _magCalib[0] - _mBias[0];
		(*my) = (float) data[1] * _mRes * _magCalib[1] - _mBias[1];
		(*mz) = (float) data[2] * _mRes * _magCalib[2] - _mBias[2];
		(*mx) *= _mScale[0];
		(*my) *= _mScale[1];
		(*mz) *= _mScale[2];
	}
}

void MPU9250SPI::writeAK8963Register(uint8_t reg, uint8_t data,
		uint32_t delay) {
	writeRegister( I2C_SLV0_ADDR, (uint8_t) 0x7F & AK8963_ADDRESS, false);
	writeRegister( I2C_SLV0_REG, reg, false);
	writeRegister( I2C_SLV0_DO, data, false);
	writeRegister( I2C_SLV0_CTRL, (uint8_t) 0x81, false);
	_delayMsFunc(delay);
}

uint8_t MPU9250SPI::readAK8963Register(uint8_t reg, uint32_t delay) {
	writeRegister( I2C_SLV0_ADDR, (uint8_t) 0x80 | AK8963_ADDRESS, false);
	writeRegister( I2C_SLV0_REG, reg, false);
	writeRegister( I2C_SLV0_CTRL, (uint8_t) 0x81, false);
	_delayMsFunc(delay);
	return readRegister( EXT_SENS_DATA_00);
}

void MPU9250SPI::readAK8963Registers(uint8_t reg, uint8_t count, uint8_t *dest,
		uint32_t delay) {
	writeRegister( I2C_SLV0_ADDR, 0x80 | AK8963_ADDRESS, false);
	writeRegister( I2C_SLV0_REG, reg, false);
	writeRegister( I2C_SLV0_CTRL, (uint8_t) 0x80 | ((uint8_t) 0x0F & count),
			false);
	_delayMsFunc(delay);
	readRegisters(EXT_SENS_DATA_00, count, dest);
}

void MPU9250SPI::writeRegister(uint8_t address, uint8_t data, bool verify) {
	address = 0x7F & address;
	uint8_t outByte[2] = { address, data };
	uint8_t inByte[2] = { 0, 0 };
	_csFunc(false);
	HAL_SPI_TransmitReceive(&mpuSpi, outByte, inByte, 2, HAL_MAX_DELAY);
	_csFunc(true);
	if (verify && readRegister(address) != data) {
		if (_errFunc) {
			_errFunc();
		}
	}
}

uint8_t MPU9250SPI::readRegister(uint8_t address) {
	address = 0x80 | address;
	uint8_t outByte[2] = { address, 0 };
	uint8_t inByte[2] = { 0, 0 };
	_csFunc(false);
	HAL_SPI_TransmitReceive(&mpuSpi, outByte, inByte, 2, HAL_MAX_DELAY);
	_csFunc(true);
	return inByte[1];
}

void MPU9250SPI::readRegisters(uint8_t address, uint8_t count, uint8_t *dest) {
	address = 0x80 | address;
	uint8_t outByte[16] =
			{ address, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	uint8_t inByte[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	_csFunc(false);
	HAL_SPI_TransmitReceive(&mpuSpi, outByte, inByte, count + 1, HAL_MAX_DELAY);
	_csFunc(true);
	memcpy(dest, &inByte[1], count);
}
