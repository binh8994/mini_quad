/*
 * ppm.h
 *
 *  Created on: Apr 25, 2020
 *      Author: thanhbinh89
 */

#ifndef __PPM_H__
#define __PPM_H__

#include <stdint.h>
#include "tim.h"

#define PPM_MAX_CHANNEL_SUPPORT (16)

class PPM {
public:
	enum {
		ROLL, PITCH, THROTTLE, YAW, SET_OFFSET, CH6, CH7, CH8
	};

	PPM(uint32_t maxChannel);
	void installDriver(TIM_HandleTypeDef *tim, uint32_t timChannel);
	void start();
	uint16_t getValue(uint32_t channel);
	int32_t getValue(uint32_t channel, int32_t min, int32_t max);
	void interruptCallback();

	static int32_t map(int32_t value, int32_t input_min, int32_t input_max,
			int32_t output_min, int32_t output_max);

private:
	TIM_HandleTypeDef *_tim;
	volatile uint32_t _timChannel;

	volatile uint32_t _ic2LastValue;
	volatile uint32_t _channel;
	volatile uint32_t _values[PPM_MAX_CHANNEL_SUPPORT];
	volatile uint32_t _maxChannel;
};

#endif /* __PPM_H__ */
