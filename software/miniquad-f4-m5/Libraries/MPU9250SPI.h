#ifndef __MPU9250_H__
#define __MPU9250_H__

#include <stdint.h>
#include "main.h"
#include "spi.h"

/* MPU-9250 Register Map and Descriptions, Revision 4.0, RM-MPU-9250A-00, Rev. 1.4, 9/9/2013 */

#define WHO_AM_I_AK8963  (uint8_t)0x00 // should return 0x48
#define INFO             (uint8_t)0x01
#define AK8963_ST1       (uint8_t)0x02  // data ready status bit 0
#define AK8963_XOUT_L    (uint8_t)0x03  // data
#define AK8963_XOUT_H    (uint8_t)0x04
#define AK8963_YOUT_L    (uint8_t)0x05
#define AK8963_YOUT_H    (uint8_t)0x06
#define AK8963_ZOUT_L    (uint8_t)0x07
#define AK8963_ZOUT_H    (uint8_t)0x08
#define AK8963_ST2       (uint8_t)0x09  // Data overflow bit 3 and data read error status bit 2
#define AK8963_CNTL      (uint8_t)0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define AK8963_CNTL2     (uint8_t)0x0B  // Reset
#define AK8963_ASTC      (uint8_t)0x0C  // Self test control
#define AK8963_I2CDIS    (uint8_t)0x0F  // I2C disable
#define AK8963_ASAX      (uint8_t)0x10  // Fuse ROM x-axis sensitivity adjustment value
#define AK8963_ASAY      (uint8_t)0x11  // Fuse ROM y-axis sensitivity adjustment value
#define AK8963_ASAZ      (uint8_t)0x12  // Fuse ROM z-axis sensitivity adjustment value

#define SELF_TEST_X_GYRO (uint8_t)0x00
#define SELF_TEST_Y_GYRO (uint8_t)0x01
#define SELF_TEST_Z_GYRO (uint8_t)0x02

#define SELF_TEST_X_ACCEL (uint8_t)0x0D
#define SELF_TEST_Y_ACCEL (uint8_t)0x0E
#define SELF_TEST_Z_ACCEL (uint8_t)0x0F

#define SELF_TEST_A      (uint8_t)0x10

#define XG_OFFSET_H      (uint8_t)0x13  // User-defined trim values for gyroscope
#define XG_OFFSET_L      (uint8_t)0x14
#define YG_OFFSET_H      (uint8_t)0x15
#define YG_OFFSET_L      (uint8_t)0x16
#define ZG_OFFSET_H      (uint8_t)0x17
#define ZG_OFFSET_L      (uint8_t)0x18
#define SMPLRT_DIV       (uint8_t)0x19
#define CONFIG           (uint8_t)0x1A
#define GYRO_CONFIG      (uint8_t)0x1B
#define ACCEL_CONFIG     (uint8_t)0x1C
#define ACCEL_CONFIG2    (uint8_t)0x1D
#define LP_ACCEL_ODR     (uint8_t)0x1E
#define WOM_THR          (uint8_t)0x1F

#define MOT_DUR          (uint8_t)0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define ZMOT_THR         (uint8_t)0x21  // Zero-motion detection threshold bits [7:0]
#define ZRMOT_DUR        (uint8_t)0x22  // Duration counter threshold for zero motion interrupt generation, 16 Hz rate, LSB = 64 ms

#define FIFO_EN          (uint8_t)0x23
#define I2C_MST_CTRL     (uint8_t)0x24
#define I2C_SLV0_ADDR    (uint8_t)0x25
#define I2C_SLV0_REG     (uint8_t)0x26
#define I2C_SLV0_CTRL    (uint8_t)0x27
#define I2C_SLV1_ADDR    (uint8_t)0x28
#define I2C_SLV1_REG     (uint8_t)0x29
#define I2C_SLV1_CTRL    (uint8_t)0x2A
#define I2C_SLV2_ADDR    (uint8_t)0x2B
#define I2C_SLV2_REG     (uint8_t)0x2C
#define I2C_SLV2_CTRL    (uint8_t)0x2D
#define I2C_SLV3_ADDR    (uint8_t)0x2E
#define I2C_SLV3_REG     (uint8_t)0x2F
#define I2C_SLV3_CTRL    (uint8_t)0x30
#define I2C_SLV4_ADDR    (uint8_t)0x31
#define I2C_SLV4_REG     (uint8_t)0x32
#define I2C_SLV4_DO      (uint8_t)0x33
#define I2C_SLV4_CTRL    (uint8_t)0x34
#define I2C_SLV4_DI      (uint8_t)0x35
#define I2C_MST_STATUS   (uint8_t)0x36
#define INT_PIN_CFG      (uint8_t)0x37
#define INT_ENABLE       (uint8_t)0x38
#define DMP_INT_STATUS   (uint8_t)0x39  // Check DMP interrupt
#define INT_STATUS       (uint8_t)0x3A
#define ACCEL_XOUT_H     (uint8_t)0x3B
#define ACCEL_XOUT_L     (uint8_t)0x3C
#define ACCEL_YOUT_H     (uint8_t)0x3D
#define ACCEL_YOUT_L     (uint8_t)0x3E
#define ACCEL_ZOUT_H     (uint8_t)0x3F
#define ACCEL_ZOUT_L     (uint8_t)0x40
#define TEMP_OUT_H       (uint8_t)0x41
#define TEMP_OUT_L       (uint8_t)0x42
#define GYRO_XOUT_H      (uint8_t)0x43
#define GYRO_XOUT_L      (uint8_t)0x44
#define GYRO_YOUT_H      (uint8_t)0x45
#define GYRO_YOUT_L      (uint8_t)0x46
#define GYRO_ZOUT_H      (uint8_t)0x47
#define GYRO_ZOUT_L      (uint8_t)0x48
#define EXT_SENS_DATA_00 (uint8_t)0x49
#define EXT_SENS_DATA_01 (uint8_t)0x4A
#define EXT_SENS_DATA_02 (uint8_t)0x4B
#define EXT_SENS_DATA_03 (uint8_t)0x4C
#define EXT_SENS_DATA_04 (uint8_t)0x4D
#define EXT_SENS_DATA_05 (uint8_t)0x4E
#define EXT_SENS_DATA_06 (uint8_t)0x4F
#define EXT_SENS_DATA_07 (uint8_t)0x50
#define EXT_SENS_DATA_08 (uint8_t)0x51
#define EXT_SENS_DATA_09 (uint8_t)0x52
#define EXT_SENS_DATA_10 (uint8_t)0x53
#define EXT_SENS_DATA_11 (uint8_t)0x54
#define EXT_SENS_DATA_12 (uint8_t)0x55
#define EXT_SENS_DATA_13 (uint8_t)0x56
#define EXT_SENS_DATA_14 (uint8_t)0x57
#define EXT_SENS_DATA_15 (uint8_t)0x58
#define EXT_SENS_DATA_16 (uint8_t)0x59
#define EXT_SENS_DATA_17 (uint8_t)0x5A
#define EXT_SENS_DATA_18 (uint8_t)0x5B
#define EXT_SENS_DATA_19 (uint8_t)0x5C
#define EXT_SENS_DATA_20 (uint8_t)0x5D
#define EXT_SENS_DATA_21 (uint8_t)0x5E
#define EXT_SENS_DATA_22 (uint8_t)0x5F
#define EXT_SENS_DATA_23 (uint8_t)0x60
#define MOT_DETECT_STATUS(uint8_t) 0x61
#define I2C_SLV0_DO      (uint8_t)0x63
#define I2C_SLV1_DO      (uint8_t)0x64
#define I2C_SLV2_DO      (uint8_t)0x65
#define I2C_SLV3_DO      (uint8_t)0x66
#define I2C_MST_DELAY_CTRL (uint8_t)0x67
#define SIGNAL_PATH_RESET  (uint8_t)0x68
#define MOT_DETECT_CTRL  (uint8_t)0x69
#define USER_CTRL        (uint8_t)0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define PWR_MGMT_1       (uint8_t)0x6B // Device defaults to the SLEEP mode
#define PWR_MGMT_2       (uint8_t)0x6C
#define DMP_BANK         (uint8_t)0x6D  // Activates a specific bank in the DMP
#define DMP_RW_PNT       (uint8_t)0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define DMP_REG          (uint8_t)0x6F  // Register in DMP from which to read or to which to write
#define DMP_REG_1        (uint8_t)0x70
#define DMP_REG_2        (uint8_t)0x71
#define FIFO_COUNTH      (uint8_t)0x72
#define FIFO_COUNTL      (uint8_t)0x73
#define FIFO_R_W         (uint8_t)0x74
#define WHO_AM_I_MPU9250 (uint8_t)0x75 // Should return 0x71
#define XA_OFFSET_H      (uint8_t)0x77
#define XA_OFFSET_L      (uint8_t)0x78
#define YA_OFFSET_H      (uint8_t)0x7A
#define YA_OFFSET_L      (uint8_t)0x7B
#define ZA_OFFSET_H      (uint8_t)0x7D
#define ZA_OFFSET_L      (uint8_t)0x7E

#define  AFS_2G  (uint8_t)0x00
#define  AFS_4G  (uint8_t)0x01
#define  AFS_8G  (uint8_t)0x02
#define  AFS_16G (uint8_t)0x03

#define  GFS_250DPS  (uint8_t)0x00
#define  GFS_500DPS  (uint8_t)0x01
#define  GFS_1000DPS (uint8_t)0x02
#define  GFS_2000DPS (uint8_t)0x03

#define  MFS_14BITS  (uint8_t)0x00 // 0.6 mG per LSB
#define  MFS_16BITS  (uint8_t)0x01    // 0.15 mG per LSB

#define M_PD   	(uint8_t)0x00
#define M_8Hz   (uint8_t)0x02
#define M_100Hz (uint8_t)0x06

#define MPU9250_ADDRESS (uint8_t)0x68
#define AK8963_ADDRESS  (uint8_t)0x0C

class MPU9250SPI {
public:
	MPU9250SPI(void (*csFunc)(bool), void (*delayMsFunc)(uint32_t), void (*errFunc)(void) = NULL);
	bool checkConnect();
	void enableI2CMaster();
	void resetAccelGyro();

	void configAccelGyro(uint8_t aScale, uint8_t gScale, uint8_t sampleRateDiv);
	void calibAccelGyro(float *gBias, float *aBias);
	void setAccelBias(float *aBias);
	void setGyroBias(float *gBias);
	bool checkAccelGyroAvalaible();
	void readAccelGyro(float *ax, float *ay, float *az, float *gx, float *gy,
				float *gz);
	void selfTestAccelGyro(float *result);

	bool checkMagConnect();
	void readMagFactoryCalib(float *magCalib);
	void configMag(uint8_t mScale, uint8_t mMode);
	void calManualMag(uint8_t mMode, float *magBias, float *magScale);
	void setMagBias(float *mBias);
	void setMagScale(float *mScale);
	bool checkMagAvalaible();
	void readMag(float *mx, float *my, float *mz);
	void setReadMagContinuous();
	void readMagContinuous(float *mx, float *my, float *mz);

private:
	float _aRes = 2.0f / 32768.0f; //AFS_2G
	float _gRes = 250.0 / 32768.0; //GFS_250DPS
	float _mRes = 10.0 * 4912.0 / 8190.0; //MFS_14BITS
	float _aBias[3] = { 0, 0, 0 }, _gBias[3] = { 0, 0, 0 };
	float _mScale[3] = { 1, 1, 1 };
	float _mBias[3] = { 0, 0, 0 };
	float _magCalib[3] = { 1, 1, 1 };
	void (*_csFunc)(bool);
	void (*_delayMsFunc)(uint32_t);
	void (*_errFunc)(void);

	void writeAK8963Register(uint8_t reg, uint8_t data, uint32_t delay = 5);
	uint8_t readAK8963Register(uint8_t reg, uint32_t delay = 5);
	void readAK8963Registers(uint8_t reg, uint8_t count, uint8_t *dest, uint32_t delay = 5);
	void writeRegister(uint8_t reg, uint8_t data, bool verify = true);
	uint8_t readRegister(uint8_t reg);
	void readRegisters(uint8_t reg, uint8_t count, uint8_t *dest);

};

#endif //__MPU9250_H__
