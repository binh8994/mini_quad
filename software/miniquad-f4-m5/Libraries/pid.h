#ifndef __PID__H__
#define __PID__H__

class PID {
public:
	PID(float p, float i, float d);
	float calculate(float input, float setpoint,
			float dT);
private:
	float _pGain;
	float _iGain;
	float _dGain;

	float _pMem;
	float _iMem;
	float _dMem;
	float _preError;
	float _prePreError;
	float _preOutput;
};

#endif // __PID_H__
