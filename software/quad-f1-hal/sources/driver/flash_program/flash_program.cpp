#include <string.h>
#include "flash_program.h"

#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"

/*private function*/
internal_flash_status_t  internal_flash_erase(uint32_t address, uint32_t len) {
	uint32_t num_of_page = len / io_get_internal_page_size() + ((len % io_get_internal_page_size()) > 0);
	return io_internal_flash_erase_page(address, num_of_page);
}

/*publich function*/
internal_flash_status_t internal_flash_read(uint32_t address, uint8_t* pbuf, uint32_t len) {
	return INTERNAL_FLASH_DRIVER_OK;
	memcpy((uint8_t*)pbuf, (uint8_t*)address, len);
	return INTERNAL_FLASH_DRIVER_OK;
}

internal_flash_status_t	internal_flash_write(uint32_t address, uint8_t* pbuf, uint32_t len) {
	return INTERNAL_FLASH_DRIVER_OK;
	internal_flash_status_t ret = INTERNAL_FLASH_DRIVER_NG;
	io_internal_flash_unlock();
	internal_flash_erase(address, len);
	ret = io_internal_flash_write(address, (uint8_t*)pbuf, len);
	io_internal_flash_lock();
	return ret;
}

