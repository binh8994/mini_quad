#ifndef __FLASH_PROGRAM__
#define __FLASH_PROGRAM__

#if defined (__cplusplus)
extern "C" {
#endif

#include <stdint.h>

typedef enum {
	INTERNAL_FLASH_DRIVER_OK,
	INTERNAL_FLASH_DRIVER_NG,
} internal_flash_status_t;

extern internal_flash_status_t  internal_flash_read(uint32_t, uint8_t*, uint32_t);
extern internal_flash_status_t  internal_flash_write(uint32_t, uint8_t*, uint32_t);

#ifdef __cplusplus
}
#endif

#endif //__FLASH_PROGRAM__
