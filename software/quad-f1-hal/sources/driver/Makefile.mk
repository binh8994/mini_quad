CFLAGS		+= -I./sources/driver/led
#CFLAGS		+= -I./sources/driver/rtc
CFLAGS		+= -I./sources/driver/button
#CFLAGS		+= -I./sources/driver/kalman

#CPPFLAGS	+= -I./sources/driver/epprom
CPPFLAGS	+= -I./sources/driver/flash_program

VPATH += sources/driver/led
VPATH += sources/driver/button
#VPATH += sources/driver/rtc
#VPATH += sources/driver/eeprom
#VPATH += sources/driver/kalman
VPATH += sources/driver/flash_program

SOURCES += sources/driver/led/led.c
SOURCES += sources/driver/button/button.c
#SOURCES += sources/driver/kalman/kalman.c

#SOURCES_CPP += sources/driver/eeprom/eeprom.cpp
SOURCES_CPP += sources/driver/flash_program/flash_program.cpp
