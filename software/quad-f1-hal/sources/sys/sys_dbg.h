/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYS_DBG_H__
#define __SYS_DBG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#if defined(TIVA_PLATFORM)
#include <stdbool.h>
#include <stdint.h>
#include "../platform/tiva/utils/uartstdio.h"

#if defined(SYS_DBG_EN)
#define SYS_DBG(fmt, ...)       UARTprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_DBG(fmt, ...)
#endif

#if defined(SYS_PRINT_EN)
#define SYS_PRINT(fmt, ...)       UARTprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_PRINT(fmt, ...)
#endif
#endif

#if defined(STM32L_PLATFORM) || defined(STM32F10X_PLATFORM)
#include "../common/xprintf.h"

#if defined(SYS_DBG_EN)
#define SYS_DBG(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_DBG(fmt, ...)
#endif

#if defined(SYS_PRINT_EN)
#define SYS_PRINT(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_PRINT(fmt, ...)
#endif
#endif

#define FATAL(a, b)	while (1)

#ifdef __cplusplus
}
#endif

#endif //__SYS_DBG_H__
