#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <inttypes.h>

extern void sys_irq_timer_10ms();
extern void sys_irq_shell(uint8_t*, uint32_t);
extern void sys_irq_nrf24l01();
extern void sys_irq_1ms();

#ifdef __cplusplus
}
#endif

#endif // __SYS_IRQ_H__
