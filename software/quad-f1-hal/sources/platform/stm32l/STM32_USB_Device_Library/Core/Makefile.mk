CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Core/Inc

VPATH += sources/platform/stm32l/STM32_USB_Device_Library/Core/Src

CPPFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Core/Inc

# C source files
SOURCES += ./sources/platform/stm32l/STM32_USB_Device_Library/Core/Src/usbd_core.c
SOURCES += ./sources/platform/stm32l/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c
SOURCES += ./sources/platform/stm32l/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c
