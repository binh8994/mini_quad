CFLAGS += -I./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Inc
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Inc/Legacy
CFLAGS += -I./sources/platform/stm32l

CPPFLAGS += -I./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Inc

VPATH += sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src
VPATH += sources/platform/stm32l

# C source files
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_cortex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_rcc_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_dma.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_gpio.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pcd_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_pwr_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_uart.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_usart.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_tim_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_spi_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_iwdg.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ex.c
SOURCES += ./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_hal_flash_ramfunc.c
