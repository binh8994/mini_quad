/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYS_CFG_H__
#define __SYS_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32l1xx_hal.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"

#define USART_RX_BUF_LEN			1

extern USBD_HandleTypeDef hUsbDeviceFS;
extern UART_HandleTypeDef sys_huart;
extern IWDG_HandleTypeDef sys_hiwdg;
extern TIM_HandleTypeDef sys_hswdg;

extern uint8_t sys_usart_rx_buf[USART_RX_BUF_LEN];

#define SYS_UART					USART1
#define SYS_UART_TX_PIN				GPIO_PIN_9
#define SYS_UART_RX_PIN				GPIO_PIN_10
#define SYS_UART_TXRX_PORT			GPIOA
#define SYS_UART_GPIO_AF			GPIO_AF7_USART1
#define SYS_UART_IRQn				USART1_IRQn
#define __SYS_UART_IO_CLK_ENABLE	__HAL_RCC_GPIOA_CLK_ENABLE

extern void sys_cfg_clock();
extern void sys_cfg_tick();
extern void sys_cfg_console();
extern void sys_deinit_console_non_it();
extern void sys_cfg_usb();
extern void sys_cfg_update_info();


#ifdef __cplusplus
}
#endif

#endif //__SYS_CFG_H__
