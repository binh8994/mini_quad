-include sources/platform/stm32l/arduino/Makefile.mk
-include sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Makefile.mk
-include sources/platform/stm32l/ST_Drivers/CMSIS/Makefile.mk
-include sources/platform/stm32l/usb_cdc_cfg/Makefile.mk
-include sources/platform/stm32l/STM32_USB_Device_Library/Class/CDC/Makefile.mk
-include sources/platform/stm32l/STM32_USB_Device_Library/Core/Makefile.mk

LDFILE = sources/platform/stm32l/ak.ld

CFLAGS += -I./sources/platform/stm32l
CFLAGS += -I./sources/platform/stm32l/ST_Drivers
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS/Device
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS/Device/ST
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS/Device/ST/STM32L1xx
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS/Device/ST/STM32L1xx/Include
CFLAGS += -I./sources/platform/stm32l/ST_Drivers/CMSIS/Include

CFLAGS += -I./sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Inc

CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Class
CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Class/CDC
CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Class/CDC/Inc
CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Core
CFLAGS += -I./sources/platform/stm32l/STM32_USB_Device_Library/Core/Inc

CPPFLAGS += -I./sources/platform/stm32l

VPATH += sources/platform/stm32l
VPATH += sources/platform/stm32l/ST_Drivers/STM32L1xx_HAL_Driver/Src
VPATH += sources/platform/stm32l/usb_cdc_cfg/Src

# C source files
SOURCES += sources/platform/stm32l/stm32l.c
SOURCES += sources/platform/stm32l/system.c
SOURCES += sources/platform/stm32l/sys_cfg.c
SOURCES += sources/platform/stm32l/io_cfg.c
SOURCES += sources/platform/stm32l/stm32l1xx_hal_msp.c
SOURCES += sources/platform/stm32l/system_stm32l1xx.c
