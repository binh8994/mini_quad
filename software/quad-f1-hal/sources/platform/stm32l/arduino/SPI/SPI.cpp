#include "SPI.h"
#include "stm32l1xx_hal.h"
#include "../../io_cfg.h"

#include "../sys/sys_dbg.h"

#define NOT_ACTIVE      (0xFF)

SPIClass::SPIClass(void) {
	SSIModule = NOT_ACTIVE;
	SSIBitOrder = MSBFIRST;
}

SPIClass::SPIClass(uint8_t module) {
	SSIModule = module;
	SSIBitOrder = MSBFIRST;
}

void SPIClass::begin() {
	__SPIx_IO_CLK();

	/*!< SPI Config */
	hspix.Instance = SPIx;
	hspix.Init.Mode = SPI_MODE_MASTER;
	hspix.Init.Direction = SPI_DIRECTION_2LINES;
	hspix.Init.DataSize = SPI_DATASIZE_8BIT;
	hspix.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspix.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspix.Init.NSS = SPI_NSS_SOFT;
	hspix.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspix.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspix.Init.TIMode = SPI_TIMODE_DISABLE;
	hspix.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspix.Init.CRCPolynomial = 10;
	if( HAL_SPI_Init(&hspix) != HAL_OK) {
		FATAL("HAL_SPI", 0x01);
	}
}

void SPIClass::end() {
	/* disable SPI module */
}

void SPIClass::setBitOrder(uint8_t ssPin, uint8_t bitOrder) {
	SSIBitOrder = bitOrder;
}

void SPIClass::setBitOrder(uint8_t bitOrder) {
	SSIBitOrder = bitOrder;
}

void SPIClass::setDataMode(uint8_t mode) {
	/*
	SPI_MODE0	0	0	Falling	Rising
	SPI_MODE1	0	1	Rising	Falling
	SPI_MODE2	1	0	Rising	Falling
	SPI_MODE3	1	1	Falling	Rising
	*/

	switch(mode) {
	case SPI_MODE0:
		hspix.Init.CLKPolarity = SPI_POLARITY_LOW;
		hspix.Init.CLKPhase = SPI_PHASE_1EDGE;
		break;

	case SPI_MODE1:
		hspix.Init.CLKPolarity = SPI_POLARITY_LOW;
		hspix.Init.CLKPhase = SPI_PHASE_2EDGE;
		break;

	case SPI_MODE2:
		hspix.Init.CLKPolarity = SPI_POLARITY_HIGH;
		hspix.Init.CLKPhase = SPI_PHASE_1EDGE;
		break;

	case SPI_MODE3:
		hspix.Init.CLKPolarity = SPI_POLARITY_HIGH;
		hspix.Init.CLKPhase = SPI_PHASE_2EDGE;
		break;

	default:
		hspix.Init.CLKPolarity = SPI_POLARITY_LOW;
		hspix.Init.CLKPhase = SPI_PHASE_1EDGE;
		break;
	}

	if( HAL_SPI_Init(&hspix) != HAL_OK) {
		FATAL("HAL_SPI", 0x02);
	}
}

void SPIClass::setClockDivider(uint8_t divider){
	hspix.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	if( HAL_SPI_Init(&hspix) != HAL_OK) {
		FATAL("HAL_SPI", 0x03);
	}
}

uint8_t SPIClass::transfer(uint8_t data) {
	unsigned long txData = data, rxData;

	if(SSIBitOrder == LSBFIRST) {
		asm("rbit %0, %1" : "=r" (txData) : "r" (txData));	// reverse order of 32 bits
		asm("rev %0, %1" : "=r" (txData) : "r" (txData));	// reverse order of bytes to get original bits into lowest byte
	}

	if(HAL_SPI_TransmitReceive(&hspix, (uint8_t*)&txData, (uint8_t*)&rxData, 1, 1000) != HAL_OK ) {
		FATAL("HAL_SPI", 0x04);
	}

	if (SSIBitOrder == LSBFIRST) {
		asm("rbit %0, %1" : "=r" (rxData) : "r" (rxData));	// reverse order of 32 bits
		asm("rev %0, %1" : "=r" (rxData) : "r" (rxData));	// reverse order of bytes to get original bits into lowest byte
	}

	return (uint8_t)rxData;
}

void SPIClass::setModule(uint8_t module) {
	SSIModule = module;
	begin();
}

SPIClass SPI;
