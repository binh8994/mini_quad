-include sources/platform/stm32f10x/arduino/Makefile.mk
-include sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Makefile.mk
-include sources/platform/stm32f10x/ST_Drivers/CMSIS/Makefile.mk
-include sources/platform/stm32f10x/usb_cdc_cfg/Makefile.mk
-include sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Makefile.mk
-include sources/platform/stm32f10x/STM32_USB_Device_Library/Core/Makefile.mk

LDFILE = sources/platform/stm32f10x/ak.ld

CFLAGS += -I./sources/platform/stm32f10x
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS/Device
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS/Device/ST
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS/Device/ST/STM32F1xx
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS/Device/ST/STM32F1xx/Include
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/CMSIS/Include

CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Inc

CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Class
CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC
CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Inc
CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Core
CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Core/Inc

CPPFLAGS += -I./sources/platform/stm32f10x

VPATH += sources/platform/stm32f10x
VPATH += sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src
VPATH += sources/platform/stm32f10x/usb_cdc_cfg/Src

# C source files
SOURCES += sources/platform/stm32f10x/stm32.c
SOURCES += sources/platform/stm32f10x/system.c
SOURCES += sources/platform/stm32f10x/sys_cfg.c
SOURCES += sources/platform/stm32f10x/io_cfg.c
SOURCES += sources/platform/stm32f10x/stm32f1xx_hal_msp.c
SOURCES += sources/platform/stm32f10x/system_stm32f1xx.c
