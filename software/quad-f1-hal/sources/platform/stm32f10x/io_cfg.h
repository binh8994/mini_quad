/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "core_cm3.h"
#include "core_cmFunc.h"

#include "../driver/eeprom/eeprom.h"
#include "../driver/flash_program/flash_program.h"

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_PIN_13)
#define LED_LIFE_IO_PORT				(GPIOC)
#define __LED_LIFE_IO_CLK				__HAL_RCC_GPIOC_CLK_ENABLE

/****************************************************************************
 *Pin map battery
*****************************************************************************/
#define BAT_ADC_PIN						(GPIO_PIN_0)
#define BAT_ADC_PORT					(GPIOA)
#define __BAT_ADC_IO_CLOCK				__HAL_RCC_GPIOA_CLK_ENABLE

/****************************************************************************
 *ADC1 cfg
*****************************************************************************/
#define ADCx							(ADC1)
#define __ADCx_CLOCK					__HAL_RCC_ADC1_CLK_ENABLE
#define BAT_ADC_CHANNEL					(ADC_CHANNEL_0)

/*****************************************************************************
 *Pin map nRF24l01
******************************************************************************/
#define NRF_CE_IO_PIN					(GPIO_PIN_3)
#define NRF_CE_IO_PORT					(GPIOA)
#define __NRF_CE_IO_CLOCK				__HAL_RCC_GPIOA_CLK_ENABLE

#define NRF_CSN_IO_PIN					(GPIO_PIN_4)
#define NRF_CSN_IO_PORT					(GPIOA)
#define __NRF_CSN_IO_CLOCK				__HAL_RCC_GPIOA_CLK_ENABLE

#define NRF_IRQ_IO_PIN					(GPIO_PIN_0)
#define NRF_IRQ_IO_PORT					(GPIOB)
#define __NRF_IRQ_IO_CLOCK				__HAL_RCC_GPIOB_CLK_ENABLE

/*****************************************************************************
 *Pin map spi
******************************************************************************/
#define SPIx							(SPI1)
#define SPIx_SCK_PIN					(GPIO_PIN_5)
#define SPIx_SCK_GPIO_PORT				(GPIOA)
#define SPIx_MISO_PIN					(GPIO_PIN_6)
#define SPIx_MISO_GPIO_PORT				(GPIOA)
#define SPIx_MOSI_PIN					(GPIO_PIN_7)
#define SPIx_MOSI_GPIO_PORT				(GPIOA)
#define __SPIx_IO_CLK					__HAL_RCC_GPIOA_CLK_ENABLE

/****************************************************************************
 *Define l3g4200d gyro sensor
*****************************************************************************/
#define L3G_I2C							(I2C2)
#define __L3G_I2C_CLOCK					__HAL_RCC_I2C2_CLK_ENABLE
#define L3G_I2C_PORT					(GPIOB)
#define L3G_I2C_SCL_PIN					(GPIO_PIN_10)
#define L3G_I2C_SDA_PIN					(GPIO_PIN_11)
#define __L3G_I2C_IO_CLOCK				__HAL_RCC_GPIOB_CLK_ENABLE

/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* inter flash function
*******************************************************************************/

extern uint32_t io_get_internal_flash_size();
extern uint32_t io_get_internal_page_size();
extern internal_flash_status_t io_internal_flash_unlock();
extern internal_flash_status_t io_internal_flash_lock();
extern internal_flash_status_t io_internal_flash_erase_page(uint32_t address, uint32_t len);
extern internal_flash_status_t io_internal_flash_read(uint32_t, uint8_t*, uint32_t);
extern internal_flash_status_t io_internal_flash_write(uint32_t, uint8_t*, uint32_t);

/******************************************************************************
* usb function
*******************************************************************************/
extern void usb_fake_plug();

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
extern void nrf24l01_io_ctrl_init();
extern void nrf24l01_ce_low();
extern void nrf24l01_ce_high();
extern void nrf24l01_csn_low();
extern void nrf24l01_csn_high();

/******************************************************************************
* adc function
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
/* configure adc peripheral */
extern void cfg_adc1(void);

/* adc configure io */
extern void io_cfg_adc();
extern uint16_t adc_batery_read();

/******************************************************************************
* l3g4200d function
*******************************************************************************/
extern void io_cfg_i2c2(void);
extern void cfg_i2c2(void);
extern void i2c_byte_write(uint8_t slAddr, uint8_t* pBuffer, uint8_t WriteAddr);
extern void i2c_buffer_read(uint8_t slAddr,uint8_t* pBuffer, uint8_t ReadAddr, uint8_t NumByteToRead);

/******************************************************************************
* pwm function
*******************************************************************************/
#define PWM_TIM	TIM4

#define SET_MOTOR_1(value)	(PWM_TIM->CCR1=(uint16_t)value);
#define SET_MOTOR_2(value)	(PWM_TIM->CCR2=(uint16_t)value);
#define SET_MOTOR_3(value)	(PWM_TIM->CCR3=(uint16_t)value);
#define SET_MOTOR_4(value)	(PWM_TIM->CCR4=(uint16_t)value);

extern void io_cfg_pwm();
extern void cfg_pwm();

/******************************************************************************
* timer function
*******************************************************************************/
extern TIM_HandleTypeDef tim_1ms;
extern void timer_1ms_init();

#define RST_BY_PWR	1
#define RST_BY_EXT	2
#define RST_BY_WDT	3
#define RST_BY_SOFT 4
extern uint32_t get_reset_source();


#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
