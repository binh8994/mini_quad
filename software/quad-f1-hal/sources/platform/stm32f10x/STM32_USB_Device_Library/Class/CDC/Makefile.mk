CFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Inc
CPPFLAGS += -I./sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Inc

VPATH += sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Src

# C source files
SOURCES += ./sources/platform/stm32f10x/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c
