#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "stm32.h"
#include "arduino/Arduino.h"
#include "stm32f1xx_hal.h"

#include "../sys/sys_dbg.h"

#include "../app/l3g4200d.h"

ADC_HandleTypeDef bat_adc_handle;
I2C_HandleTypeDef l3g4_i2c2;
TIM_HandleTypeDef pwm_tim;
TIM_OC_InitTypeDef oc_cfg;
TIM_HandleTypeDef tim_1ms;

/******************************************************************************
* led status function
*******************************************************************************/
void led_life_init() {
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__LED_LIFE_IO_CLK();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN, GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = LED_LIFE_IO_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED_LIFE_IO_PORT, &GPIO_InitStruct);
}

void led_life_on() {
	HAL_GPIO_WritePin(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN, GPIO_PIN_RESET);
}

void led_life_off() {
	HAL_GPIO_WritePin(LED_LIFE_IO_PORT, LED_LIFE_IO_PIN, GPIO_PIN_SET);
}

/******************************************************************************
* eeprom function
* when using function DATA_EEPROM_ProgramByte,
* must be DATA_EEPROM_unlock- DATA_EEPROM_ProgramByte- DATA_EEPROM_lock
*******************************************************************************/
uint8_t io_eeprom_read(uint32_t address, uint8_t* pbuf, uint32_t len) {

	return EEPROM_DRIVER_OK;
}

uint8_t io_eeprom_write(uint32_t address, uint8_t* pbuf, uint32_t len) {

	return EEPROM_DRIVER_OK;
}

uint8_t io_eeprom_erase(uint32_t address, uint32_t len) {

	return EEPROM_DRIVER_OK;
}

/******************************************************************************
* internal flash function
*******************************************************************************/

uint32_t io_get_internal_flash_size() {
	uint32_t flash_size = (uint32_t)((*((uint32_t *)FLASHSIZE_BASE)&0xFFFFU) * 1024U);
	return flash_size;
}

uint32_t io_get_internal_page_size() {
	return (uint32_t)256;
}

internal_flash_status_t io_internal_flash_unlock() {
	if (HAL_FLASH_Unlock() != HAL_OK) {
		return INTERNAL_FLASH_DRIVER_NG;
	}
	return INTERNAL_FLASH_DRIVER_OK;
}

internal_flash_status_t io_internal_flash_lock() {
	if ((HAL_FLASH_Lock()) != HAL_OK) {
		return INTERNAL_FLASH_DRIVER_NG;
	}
	return INTERNAL_FLASH_DRIVER_OK;
}

internal_flash_status_t io_internal_flash_erase_page(uint32_t address, uint32_t num_of_page) {
	FLASH_EraseInitTypeDef EraseInitStruct;
	EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.PageAddress = address;
	EraseInitStruct.NbPages     = num_of_page;
	if (HAL_FLASHEx_Erase(&EraseInitStruct, NULL) != HAL_OK) {
		FATAL("HAL_FLASH", 0x01);
		return INTERNAL_FLASH_DRIVER_NG;
	}
	return INTERNAL_FLASH_DRIVER_OK;
}

internal_flash_status_t io_internal_flash_write(uint32_t address, uint8_t* pbuf, uint32_t len) {
	uint32_t tmp_data;
	uint32_t index = 0;

	__HAL_FLASH_DISABLE_IT(FLASH_IT_EOP | FLASH_IT_ERR);
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGERR );

	while (index < len) {
		tmp_data = 0;
		memcpy(&tmp_data, pbuf + index, (len - index) >= sizeof(uint32_t) ? sizeof(uint32_t) : (len - index));

		if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address + index, tmp_data) != HAL_OK) {
			FATAL("HAL_FLASH", 0x02);
			return INTERNAL_FLASH_DRIVER_NG;
		}
		else {
			index += sizeof(uint32_t);

		}
	}

	return INTERNAL_FLASH_DRIVER_OK;
}

void usb_fake_plug() {
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11 | GPIO_PIN_12, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = GPIO_PIN_11 | GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_Delay(200);

	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
void nrf24l01_io_ctrl_init() {
	/* CE / CSN / IRQ */
	GPIO_InitTypeDef        GPIO_InitStructure;

	__NRF_CE_IO_CLOCK();
	__NRF_CSN_IO_CLOCK();
	__NRF_IRQ_IO_CLOCK();

	/*CE -> PA3*/
	GPIO_InitStructure.Pin = NRF_CE_IO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(NRF_CE_IO_PORT, &GPIO_InitStructure);

	/*CNS -> PA4*/
	GPIO_InitStructure.Pin = NRF_CSN_IO_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(NRF_CSN_IO_PORT, &GPIO_InitStructure);

	/* IRQ -> PB0 */
	//GPIO_InitStructure.Pin = NRF_IRQ_IO_PIN;
	//GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
	//GPIO_InitStructure.Pull = GPIO_PULLUP;
	//GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	//HAL_GPIO_Init(NRF_IRQ_IO_PORT, &GPIO_InitStructure);

	//HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	//HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void nrf24l01_ce_low() {
	HAL_GPIO_WritePin(NRF_CE_IO_PORT, NRF_CE_IO_PIN, GPIO_PIN_RESET);
}

void nrf24l01_ce_high() {
	HAL_GPIO_WritePin(NRF_CE_IO_PORT, NRF_CE_IO_PIN, GPIO_PIN_SET);
}

void nrf24l01_csn_low() {
	HAL_GPIO_WritePin(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN, GPIO_PIN_RESET);
}

void nrf24l01_csn_high() {
	HAL_GPIO_WritePin(NRF_CSN_IO_PORT, NRF_CSN_IO_PIN, GPIO_PIN_SET);
}

/******************************************************************************
* adc function
* + CT sensor
* + themistor sensor
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
void cfg_adc1(void) {
	ADC_ChannelConfTypeDef ADC_Channel;

	__BAT_ADC_IO_CLOCK();
	__ADCx_CLOCK();

	bat_adc_handle.Instance = ADCx;
	bat_adc_handle.Init.ScanConvMode = ADC_SCAN_DISABLE;
	bat_adc_handle.Init.ContinuousConvMode = DISABLE;
	bat_adc_handle.Init.DiscontinuousConvMode = DISABLE;
	bat_adc_handle.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	bat_adc_handle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	bat_adc_handle.Init.NbrOfConversion = 1;
	HAL_ADC_Init(&bat_adc_handle);

	ADC_Channel.Channel = BAT_ADC_CHANNEL;
	ADC_Channel.Rank = 1;
	ADC_Channel.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;

	if (HAL_ADC_ConfigChannel(&bat_adc_handle, &ADC_Channel) != HAL_OK) {
		FATAL("HAL_ADC", 0x01);
	}

}

void io_cfg_adc() {
	GPIO_InitTypeDef    GPIO_InitStructure;

	__BAT_ADC_IO_CLOCK();
	GPIO_InitStructure.Pin = BAT_ADC_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(BAT_ADC_PORT, &GPIO_InitStructure);
}

uint16_t adc_batery_read() {
	uint16_t ret = 0;
	HAL_ADC_Start(&bat_adc_handle);

	HAL_ADC_PollForConversion(&bat_adc_handle, 1000);
	ret = (uint16_t)HAL_ADC_GetValue(&bat_adc_handle);

	HAL_ADC_Stop(&bat_adc_handle);
	return ret;
}

void io_cfg_i2c2() {
	GPIO_InitTypeDef GPIO_InitStruct;
	__L3G_I2C_IO_CLOCK();

	GPIO_InitStruct.Pin = L3G_I2C_SCL_PIN | L3G_I2C_SDA_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(L3G_I2C_PORT, &GPIO_InitStruct);
}

void cfg_i2c2(void) {
	__L3G_I2C_CLOCK();

	l3g4_i2c2.Instance = L3G_I2C;
	l3g4_i2c2.Init.ClockSpeed = 100000;
	l3g4_i2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
	l3g4_i2c2.Init.OwnAddress1 = 0;
	l3g4_i2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	l3g4_i2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	l3g4_i2c2.Init.OwnAddress2 = 0;
	l3g4_i2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	l3g4_i2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&l3g4_i2c2) != HAL_OK) {
		FATAL("HAL_I2C", 0x01);
	}

}

/**
* @brief  Writes one byte to the  L3G4200D.
* @param  slAddr : slave address L3G_I2C_ADDRESS
* @param  pBuffer : pointer to the buffer  containing the data to be written to the L3G4200D.
* @param  WriteAddr : address of the register in which the data will be written
* @retval None
*/
void i2c_byte_write(uint8_t slAddr, uint8_t* pBuffer, uint8_t WriteAddr) {
	ENTRY_CRITICAL();
	HAL_StatusTypeDef stt = HAL_I2C_Mem_Write(&l3g4_i2c2, slAddr,  WriteAddr, I2C_MEMADD_SIZE_8BIT, pBuffer, 1, 10000 );
	if (stt != HAL_OK) {
		FATAL("HAL_I2C", 0x02);
	}
	EXIT_CRITICAL();
}

/**
* @brief  Reads a block of data from the L3G4200D.
* @param  slAddr  : slave address L3G_I2C_ADDRESS
* @param  pBuffer : pointer to the buffer that receives the data read from the L3G4200D.
* @param  ReadAddr : L3G4200D's internal address to read from.
* @param  NumByteToRead : number of bytes to read from the L3G4200D.
* @retval None
*/

void i2c_buffer_read(uint8_t slAddr,uint8_t* pBuffer, uint8_t ReadAddr, uint8_t NumByteToRead) {
	ENTRY_CRITICAL();
	if (HAL_I2C_Mem_Read(&l3g4_i2c2, slAddr,  ReadAddr, I2C_MEMADD_SIZE_8BIT, pBuffer, NumByteToRead, NumByteToRead*10 ) != HAL_OK) {
		FATAL("HAL_I2C", 0x03);
	}
	EXIT_CRITICAL();
}

void io_cfg_pwm() {
	GPIO_InitTypeDef   GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/* Common configuration for all channels */
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void cfg_pwm() {
	TIM_MasterConfigTypeDef sMasterConfig;

	__HAL_RCC_TIM4_CLK_ENABLE();

	/* APB1=64MHz */
	pwm_tim.Instance = PWM_TIM;
	pwm_tim.Init.Prescaler         = 16 - 1;
	pwm_tim.Init.Period            = 1000 - 1;//4kHz
	pwm_tim.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
	pwm_tim.Init.CounterMode       = TIM_COUNTERMODE_UP;
	if (HAL_TIM_PWM_Init(&pwm_tim) != HAL_OK) {
		FATAL("HAL_PWM", 0x01);
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&pwm_tim, &sMasterConfig) != HAL_OK) {
		FATAL("HAL_PWM", 0x00);
	}

	/* Configure the PWM channels */
	/* Common configuration for all channels */
	oc_cfg.OCMode       = TIM_OCMODE_PWM1;
	oc_cfg.OCPolarity   = TIM_OCPOLARITY_HIGH;
	oc_cfg.OCFastMode   = TIM_OCFAST_DISABLE;
	oc_cfg.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
	oc_cfg.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	oc_cfg.OCIdleState  = TIM_OCIDLESTATE_RESET;
	oc_cfg.Pulse = 0;

	if (HAL_TIM_PWM_ConfigChannel(&pwm_tim, &oc_cfg, TIM_CHANNEL_1) != HAL_OK) {
		FATAL("HAL_PWM", 0x02);
	}
	if (HAL_TIM_PWM_ConfigChannel(&pwm_tim, &oc_cfg, TIM_CHANNEL_2) != HAL_OK) {
		FATAL("HAL_PWM", 0x03);
	}
	if (HAL_TIM_PWM_ConfigChannel(&pwm_tim, &oc_cfg, TIM_CHANNEL_3) != HAL_OK) {
		FATAL("HAL_PWM", 0x04);
	}
	if (HAL_TIM_PWM_ConfigChannel(&pwm_tim, &oc_cfg, TIM_CHANNEL_4) != HAL_OK) {
		FATAL("HAL_PWM", 0x05);
	}

	/* Start PWM signals generation */
	if (HAL_TIM_PWM_Start(&pwm_tim, TIM_CHANNEL_1) != HAL_OK) {
		FATAL("HAL_PWM", 0x06);
	}
	if (HAL_TIM_PWM_Start(&pwm_tim, TIM_CHANNEL_2) != HAL_OK){
		FATAL("HAL_PWM", 0x07);
	}
	if (HAL_TIM_PWM_Start(&pwm_tim, TIM_CHANNEL_3) != HAL_OK){
		FATAL("HAL_PWM", 0x08);
	}
	if (HAL_TIM_PWM_Start(&pwm_tim, TIM_CHANNEL_4) != HAL_OK){
		FATAL("HAL_PWM", 0x09);
	}
}

void timer_1ms_init() {
	TIM_MasterConfigTypeDef sMasterConfig;
	//t=1ms
	tim_1ms.Instance = TIM2;
	tim_1ms.Init.Prescaler = 10 - 1;
	tim_1ms.Init.CounterMode = TIM_COUNTERMODE_UP;
	tim_1ms.Init.Period = HAL_RCC_GetHCLKFreq() / 10000 - 1;//6399;
	if (HAL_TIM_Base_Init(&tim_1ms) != HAL_OK) {
		FATAL("HAL_TIM", 0x09);
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&tim_1ms, &sMasterConfig) != HAL_OK) {
		FATAL("HAL_TIM", 0x0A);
	}

	HAL_TIM_Base_Start_IT(&tim_1ms);
}

uint32_t get_reset_source() {
	uint32_t ret;
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST)) {		
		ret = RST_BY_EXT;

	} else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST)) {
		ret = RST_BY_SOFT;

	} else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) || \
			   __HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST)) {
		ret = RST_BY_WDT;

	} else {
		ret = RST_BY_PWR;

	}
	__HAL_RCC_CLEAR_RESET_FLAGS();
	return ret;
}
