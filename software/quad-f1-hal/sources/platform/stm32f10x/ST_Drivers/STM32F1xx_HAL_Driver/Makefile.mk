CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Inc
CFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Inc/Legacy
CFLAGS += -I./sources/platform/stm32f10x

CPPFLAGS += -I./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Inc

VPATH += sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src
VPATH += sources/platform/stm32f10x

# C source files
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pcd.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pcd_ex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_usart.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi_ex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_iwdg.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_ll_usb.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_i2c.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc.c
SOURCES += ./sources/platform/stm32f10x/ST_Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc_ex.c
