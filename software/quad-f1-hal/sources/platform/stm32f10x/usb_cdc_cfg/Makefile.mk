CFLAGS += -I./sources/platform/stm32f10x/usb_cdc_cfg/Inc
VPATH += sources/platform/stm32f10x/usb_cdc_cfg/Src

CPPFLAGS += -I./sources/platform/stm32f10x/usb_cdc_cfg/Inc

# C source files
SOURCES += ./sources/platform/stm32f10x/usb_cdc_cfg/Src/usbd_cdc_if.c
SOURCES += ./sources/platform/stm32f10x/usb_cdc_cfg/Src/usbd_conf.c
SOURCES += ./sources/platform/stm32f10x/usb_cdc_cfg/Src/usbd_desc.c
