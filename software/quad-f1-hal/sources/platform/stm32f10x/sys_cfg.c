/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>

#include "sys_cfg.h"
#include "system.h"

#include "stm32f1xx.h"
#include "system_stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "core_cm3.h"
#include "core_cmFunc.h"

#include "../../sys/sys_dbg.h"
#include "../../sys/sys_ctrl.h"
#include "../../sys/sys_irq.h"
#include "../../sys/sys_io.h"

#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"

#include "../../common/xprintf.h"

#include "../../sys/sys_dbg.h"
#include "../../sys/sys_def.h"
#include "../../sys/sys_ctrl.h"

/* Private define */
static void xputchar(uint8_t c);

#ifdef DBG_BY_USART
UART_HandleTypeDef sys_huart;
uint8_t sys_usart_rx_buf[USART_RX_BUF_LEN];
#else
USBD_HandleTypeDef hUsbDeviceFS;
#endif

IWDG_HandleTypeDef sys_hiwdg;
TIM_HandleTypeDef sys_hswdg;

static uint32_t delay_coeficient = 0;

/******************************************************************************
* assert function
*******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line) {
	xprintf("file: %s, line: %d", file, line);
	FATAL("HAL_ASSERT", 0xFF);
}

/******************************************************************************
* system configure function
*******************************************************************************/
void sys_cfg_clock() {
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	HAL_RCC_DeInit();

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
	if( HAL_RCC_OscConfig(&RCC_OscInitStruct)  != HAL_OK ) {
		FATAL("HAL_CLOCK", 0x01);
	}

	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if( HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK ) {
		FATAL("HAL_CLOCK", 0x02);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
	PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		FATAL("HAL_CLOCK", 0x03);
	}

	SystemCoreClockUpdate();
}

void sys_cfg_tick() {
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000); //1ms
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}

void sys_cfg_console() {

#ifdef DBG_BY_USART
	__SYS_UART_IO_CLK_ENABLE();

	sys_huart.Instance = SYS_UART;
	sys_huart.Init.BaudRate = SYS_CONSOLE_BAUDRATE;
	sys_huart.Init.WordLength = UART_WORDLENGTH_8B;
	sys_huart.Init.StopBits = UART_STOPBITS_1;
	sys_huart.Init.Parity = UART_PARITY_NONE;
	sys_huart.Init.Mode = UART_MODE_TX_RX;
	sys_huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	sys_huart.Init.OverSampling = UART_OVERSAMPLING_16;
	if( HAL_UART_Init(&sys_huart) != HAL_OK ) {
		FATAL("HAL_UART", 0x03);
	}

	HAL_UART_Receive_IT(&sys_huart, (uint8_t*)sys_usart_rx_buf, USART_RX_BUF_LEN);
#else
	usb_fake_plug();

	USBD_Init(&hUsbDeviceFS, &FS_Desc, DEVICE_FS);
	USBD_RegisterClass(&hUsbDeviceFS, &USBD_CDC);
	USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
	USBD_Start(&hUsbDeviceFS);
#endif
	xfunc_out = xputchar;
}

void sys_deinit_console_non_it() {
#ifdef DBG_BY_USART
	if( HAL_UART_Init(&sys_huart) != HAL_OK ) {
		FATAL("HAL_UART", 0x04);
	}
#endif
}

void sys_cfg_update_info() {
	extern uint32_t _start_flash;
	extern uint32_t _end_flash;
	extern uint32_t _start_ram;
	extern uint32_t _end_ram;
	extern uint32_t _data;
	extern uint32_t _edata;
	extern uint32_t _bss;
	extern uint32_t _ebss;
	extern uint32_t __heap_start__;
	extern uint32_t __heap_end__;
	extern uint32_t _estack;

	uint32_t hclk = HAL_RCC_GetHCLKFreq();

	system_info.cpu_clock = hclk;
	system_info.tick      = 1;
	system_info.console_baudrate = SYS_CONSOLE_BAUDRATE;
	system_info.flash_used = ((uint32_t)&_end_flash - (uint32_t)&_start_flash) + ((uint32_t)&_edata - (uint32_t)&_data);
	system_info.ram_used = (uint32_t)&_end_ram - (uint32_t)&_start_ram;
	system_info.data_init_size = (uint32_t)&_edata - (uint32_t)&_data;
	system_info.data_non_init_size = (uint32_t)&_ebss - (uint32_t)&_bss;
	system_info.stack_size = (uint32_t)&_estack - (uint32_t)&_end_ram;
	system_info.heap_size = (uint32_t)&__heap_end__ - (uint32_t)&__heap_start__;

	delay_coeficient = system_info.cpu_clock /1000000;

	/* system banner */
	SYS_PRINT("system information:\r\n");
	SYS_PRINT("\tFLASH used:\t%d bytes\r\n", system_info.flash_used);
	SYS_PRINT("\tSRAM used:\t%d bytes\r\n", system_info.ram_used);
	SYS_PRINT("\t\tdata init size:\t\t%d bytes\r\n", system_info.data_init_size);
	SYS_PRINT("\t\tdata non_init size:\t%d bytes\r\n", system_info.data_non_init_size);
	SYS_PRINT("\t\tstack size:\t\t%d bytes\r\n", system_info.stack_size);
	SYS_PRINT("\t\theap size:\t\t%d bytes\r\n", system_info.heap_size);
	SYS_PRINT("\r\n");
	SYS_PRINT("\tcpu clock:\t%d Hz\r\n", system_info.cpu_clock);
	SYS_PRINT("\ttime tick:\t%d ms\r\n", system_info.tick);
	SYS_PRINT("\tconsole:\t%d bps\r\n", system_info.console_baudrate);
	SYS_PRINT("\n\r\n");
}

/******************************************************************************
* system utilities function
*******************************************************************************/
void xputchar(uint8_t c) {
#ifdef DBG_BY_USART
	if( HAL_UART_Transmit(&sys_huart, &c, 1, 1) != HAL_OK ) {
		FATAL("HAL_UART", 0x05);
	}
#else
	if( hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED) {
		CDC_TransmitBlock_FS(&c, 1);
	}
#endif
}

uint8_t sys_ctrl_shell_get_char(uint8_t* c) {
#ifdef DBG_BY_USART
	return (uint8_t)HAL_UART_Receive(&sys_huart, c, 1, 0);
#else
	return 0;
#endif
}

void sys_ctrl_reset() {
	NVIC_SystemReset();
}

void sys_ctrl_delay_us(volatile uint32_t count) {
	volatile uint32_t delay_value = 0;
	delay_value = count*delay_coeficient / 8;
	while(delay_value--);
}

void sys_ctrl_delay_ms(volatile uint32_t count) {
#if 0
	HAL_Delay(count);
#else
	volatile uint32_t delay = 1000 * count;
	sys_ctrl_delay_us(delay);
#endif
}

void sys_ctrl_independent_watchdog_init() {
	sys_hiwdg.Instance = IWDG;
	sys_hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
	sys_hiwdg.Init.Reload = 4095;
	if (HAL_IWDG_Init(&sys_hiwdg) != HAL_OK) {
		FATAL("HAL_IWDG", 0x06);
	}

	HAL_IWDG_Refresh(&sys_hiwdg);
}

void sys_ctrl_independent_watchdog_reset() {
	HAL_IWDG_Refresh(&sys_hiwdg);
}

static uint32_t sys_ctrl_soft_counter = 0;
static uint32_t sys_ctrl_soft_time_out;

void sys_ctrl_soft_watchdog_init(uint32_t time_out) {
	TIM_MasterConfigTypeDef sMasterConfig;
	sys_ctrl_soft_time_out = time_out;

	//t=1s
	sys_hswdg.Instance = TIM3;
	sys_hswdg.Init.Prescaler = 10000;
	sys_hswdg.Init.CounterMode = TIM_COUNTERMODE_UP;
	sys_hswdg.Init.Period = HAL_RCC_GetHCLKFreq() / 10000 - 1;//7199;
	if (HAL_TIM_Base_Init(&sys_hswdg) != HAL_OK) {
		FATAL("HAL_TIM", 0x07);
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&sys_hswdg, &sMasterConfig) != HAL_OK) {
		FATAL("HAL_TIM", 0x08);
	}

	HAL_TIM_Base_Start_IT(&sys_hswdg);
}

void sys_ctrl_soft_watchdog_reset() {
	//ENTRY_CRITICAL();
	sys_ctrl_soft_counter = 0;
	//EXIT_CRITICAL();
}

void sys_ctrl_soft_watchdog_enable() {
	//ENTRY_CRITICAL();
	HAL_TIM_Base_Start_IT(&sys_hswdg);
	//EXIT_CRITICAL();
}

void sys_ctrl_soft_watchdog_disable() {
	//ENTRY_CRITICAL();
	HAL_TIM_Base_Stop_IT(&sys_hswdg);
	//EXIT_CRITICAL();
}

void sys_ctrl_soft_watchdog_increase_counter() {
	sys_ctrl_soft_counter++;
	if (sys_ctrl_soft_counter >= sys_ctrl_soft_time_out) {
		FATAL("SWDG", 0x01);
	}
}
