CFLAGS		+= -I./sources/app
CPPFLAGS	+= -I./sources/app

VPATH += sources/app

# CPP source files
SOURCES_CPP += sources/app/app.cpp
SOURCES_CPP += sources/app/shell.cpp
SOURCES_CPP += sources/app/remote.cpp
SOURCES_CPP += sources/app/sensor.cpp
SOURCES_CPP += sources/app/motor.cpp
SOURCES_CPP += sources/app/mpid.cpp
SOURCES_CPP += sources/app/app_non_clear_ram.cpp
