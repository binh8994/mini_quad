#include "mpid.h"

void pid_setup(mpid_t* pid, float p, float i, float d, float max) {
	pid->p_gain = p;
	pid->i_gain = i;
	pid->d_gain = d;
	pid->max_output = max;
	pid->p_mem = 0;
	pid->i_mem = 0;
	pid->d_mem = 0;
	pid->last_d_error = 0;
}

float pid_calcu(mpid_t* pid,  float input, float setpoint) {
	float pid_error_temp = input - setpoint;

	//P
	pid->p_mem = pid->p_gain * pid_error_temp;

	//I
	pid->i_mem = pid->i_mem + (pid->i_gain * pid_error_temp);
	if (pid->i_mem > pid->max_output) pid->i_mem = pid->max_output;
	else if (pid->i_mem < (-pid->max_output)) pid->i_mem = (-pid->max_output);

	//D
	pid->d_mem =pid->d_gain * (pid_error_temp - pid->last_d_error);
	if (pid->d_mem > pid->max_output) pid->d_mem = pid->max_output;
	else if (pid->d_mem < (-pid->max_output)) pid->d_mem = (-pid->max_output);

	pid->last_d_error = pid_error_temp;

	return (pid->p_mem + pid->i_mem + pid->d_mem);

}


