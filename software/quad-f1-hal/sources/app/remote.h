#ifndef __REMOTE_H__
#define __REMOTE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

typedef struct {
	uint8_t reamble;
	uint16_t x1;
	uint16_t y1;
	uint16_t x2;
	uint16_t y2;
	uint8_t but_2 : 4;
	uint8_t but_1 : 4;
} __attribute__((__packed__)) remote_data_t;

extern void remote_init();
extern bool remote_config();
extern bool remote_read();
extern uint8_t get_button_test();

#ifdef __cplusplus
}
#endif

#endif // __REMOTE_H__
