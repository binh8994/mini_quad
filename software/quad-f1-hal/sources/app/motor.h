#ifndef __MOTOR_H__
#define __MOTOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern void motor_init();
extern void motor_config();

#ifdef __cplusplus
}
#endif

#endif // __MOTOR_H__
