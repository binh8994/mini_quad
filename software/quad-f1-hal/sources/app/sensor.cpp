#include "../sys/sys_dbg.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_arduino.h"

#include "../common/utils.h"

#include "app_dbg.h"
#include "sensor.h"
#include "l3g4200d.h"

static bool l3g4_cfg(L3G_ConfigTypeDef*);
//static void l3g4_read_buffer(uint8_t*);
static void l3g4_read_raw_data(int16_t*);
static void l3g4_read_data(float*);

sensor_t sensor_data;
static L3G_ConfigTypeDef L3G_InitStructure;
static sensor_t calib;
static float rpy[3];

void sensor_init() {
	io_cfg_i2c2();
	cfg_i2c2();
}

bool sensor_config() {

	L3G_InitStructure.ODR = L3G_ODR_100;
	L3G_InitStructure.Bandwidth = L3G_BW_Conf1;
	L3G_InitStructure.Power = L3G_PD_On;
	L3G_InitStructure.Axis_Enable = L3G_XYZEN;
	L3G_InitStructure.FS = L3G_FS_500;
	L3G_InitStructure.BDU = L3G_BDU_Single;
	L3G_InitStructure.Endianess = L3G_Little_Endian;

	return l3g4_cfg(&L3G_InitStructure);
}

void sensor_read() {
	rpy[0] = rpy[1] = rpy[2] = 0;
	l3g4_read_data(rpy);

	//APP_DBG("raw[rpy]\t%d\t%d\t%d\n",
	//	(int32_t)(rpy[0]*1000),
	//	(int32_t)(rpy[1]*1000),
	//	(int32_t)(rpy[2]*1000));

	sensor_data.roll = rpy[0] - calib.roll;
	sensor_data.pitch = rpy[1]*(-1) - calib.pitch;
	sensor_data.yaw = rpy[2]*(-1) - calib.yaw	;
}

sensor_t sensor_calib(uint32_t times) {
	rpy[0] = rpy[1] = rpy[2] = 0;
	calib.roll = calib.pitch = calib.yaw = 0;

	for(uint32_t i=0; i<times; i++) {
		l3g4_read_data(rpy);

		calib.roll	+= rpy[0];
		calib.pitch	+= rpy[1];
		calib.yaw	+= rpy[2];
	}

	calib.roll	/= times;
	calib.pitch	/= times;
	calib.yaw	/= times;

	APP_DBG("CABLIB[rpy]\t%d\t%d\t%d\n",\
			(int32_t)(calib.roll  *1000),\
			(int32_t)(calib.pitch *1000),\
			(int32_t)(calib.yaw   *1000));
	return calib;
}


bool l3g4_cfg(L3G_ConfigTypeDef *L3G_Config_Struct) {
	uint8_t whoami[1] = {0x00};
	uint8_t CRTL1 = 0x00;
	uint8_t CRTL2 = 0x00;
	uint8_t CRTL3 = 0x00;
	uint8_t CRTL4 = 0x00;

	CRTL1 |= (uint8_t) (L3G_Config_Struct->ODR | L3G_Config_Struct->Bandwidth |
						L3G_Config_Struct->Power | L3G_Config_Struct->Axis_Enable);
	CRTL4 |= (uint8_t) (L3G_Config_Struct->BDU | L3G_Config_Struct->Endianess | L3G_Config_Struct->FS);

	i2c_buffer_read(L3G_I2C_ADDRESS, whoami, L3G_WHO_AM_I, 1);
	if(whoami[0] != 0xD3 ) {
		return false;
	}

	//SYS_DBG("CRTL_REG1 0X%X\n", CRTL1);
	//SYS_DBG("CRTL_REG2 0X%X\n", CRTL2);
	//SYS_DBG("CRTL_REG3 0X%X\n", CRTL3);
	//SYS_DBG("CRTL_REG4 0X%X\n", CRTL4);

	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL1, L3G_CTRL_REG1);
	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL2, L3G_CTRL_REG2);
	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL3, L3G_CTRL_REG3);
	i2c_byte_write(L3G_I2C_ADDRESS, &CRTL4, L3G_CTRL_REG4);
	return true;
}


//void l3g4_read_buffer(uint8_t* out) {
//	i2c_buffer_read(L3G_I2C_ADDRESS,  out, L3G_OUT_X_L|0x80, 6);
//}

void l3g4_read_raw_data(int16_t* out) {
	uint8_t buffer[6];
	uint8_t low, high;

	i2c_buffer_read(L3G_I2C_ADDRESS, buffer, L3G_OUT_X_L|0x80, 6);

	low = buffer[0];
	high = buffer[1];
	out[0] = (high<<8) | low;

	low = buffer[2];
	high = buffer[3];
	out[1] = (high<<8) | low;
	out[1] = -out[1];

	low = buffer[4];
	high = buffer[5];
	out[2] = (high<<8) | low;
	out[2] = - out[2];

}

void l3g4_read_data(float* out) {
	int16_t tmp[3];
	l3g4_read_raw_data(tmp);
	out[0] = (float)tmp[0] * L3G_Gyr_Coeff;
	out[1] = (float)tmp[1] * L3G_Gyr_Coeff;
	out[2] = (float)tmp[2] * L3G_Gyr_Coeff;
}

