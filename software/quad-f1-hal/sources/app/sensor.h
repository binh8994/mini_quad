#ifndef __SENSOR_H__
#define __SENSOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "app.h"

typedef struct {
	float roll;
	float pitch;
	float yaw;
} sensor_t;

extern void sensor_init();
extern bool sensor_config();
extern sensor_t sensor_calib(uint32_t times);
extern void sensor_read();

#ifdef __cplusplus
}
#endif

#endif // __SENSOR_H__
