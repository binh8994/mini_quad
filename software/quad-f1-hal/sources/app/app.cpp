/* driver include */
#include "../driver/led/led.h"
#include "../driver/button/button.h"
#include "../driver/flash_program/flash_program.h"

/* app include */
#include "app.h"
#include "app_dbg.h"
#include "app_non_clear_ram.h"
#include "inter_flash.h"
#include "remote.h"
#include "sensor.h"
#include "motor.h"
#include "mpid.h"

/* sys include */
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_dbg.h"
#include "../sys/sys_arduino.h"

/* common include */
#include "../common/utils.h"

led_t led_life;
sensor_t sensor_input;
mpid_t pid_roll, pid_pitch, pid_yaw;
extern sensor_t sensor_data;
extern remote_data_t remote_data;

float pid_output_roll;
float pid_output_pitch;
float pid_output_yaw;
float pid_output_altitude = 0.0;

int16_t motor1, motor2, motor3, motor4;

#define MAX_THR		999
//====================[P]=======[I]=======[D]====//
//#define ROLL_PID	40000.0,	50.0,	300000.0
#define ROLL_PID	40000.0,	0.0,	300000.0
//#define PITCH_PID	40000.0,	200.0,	300000.0
#define PITCH_PID	40000.0,	0.0,	300000.0
#define YAW_PID		70000.0,	0.0,	10000.0
//====================[P]=======[I]=======[D]====//

void led_blink_blocking(led_t *led, uint32_t on, uint32_t off) {
	led_on(led); sys_ctrl_delay_ms(on);
	led_off(led); sys_ctrl_delay_ms(off);
}

int main_app() {
	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	if (reset_source_magic_number == INDICA_MACGIC_NUMBER) {
		APP_PRINT("RUN\n");
		reset_source_magic_number = ~INDICA_MACGIC_NUMBER;
		sys_ctrl_delay_ms(1000);
	} else {
		APP_PRINT("STOP\n");
		reset_source_magic_number = INDICA_MACGIC_NUMBER;
		while (1) {
			led_blink_blocking(&led_life, 500, 500);
		}
	}

	//TODO
	//io_cfg_adc();
	//cfg_adc1();

	sensor_init();
	while (!sensor_config()) {
		led_blink_blocking(&led_life, 100, 100);
	}

	remote_init();
	while (!remote_config()) {
		led_blink_blocking(&led_life, 100, 100);
	}

	while (!remote_read()/* || remote_data.but_1 || remote_data.but_2*/) {
		led_blink_blocking(&led_life, 100, 900);
	}

	sensor_calib(5000);

	motor_init();
	motor_config();

	pid_setup(&pid_roll, ROLL_PID, MAX_THR);
	pid_setup(&pid_pitch, PITCH_PID, MAX_THR);
	pid_setup(&pid_yaw, YAW_PID, MAX_THR);

	//timer_1ms_init();
	led_on(&led_life);

	while (true) {

		sensor_read();
		sensor_input.roll  = sensor_input.roll*0.8 + ((sensor_data.roll / RAD_TO_DEG) * 0.2);
		sensor_input.pitch = sensor_input.pitch*0.8 + ((sensor_data.pitch / RAD_TO_DEG) * 0.2);
		sensor_input.yaw   = sensor_input.yaw*0.8 + ((sensor_data.yaw / RAD_TO_DEG) * 0.2);

#if 0
		APP_DBG("AC[rpy]\t%d\t%d\t%d\n",
				(int32_t)(sensor_input.roll *1000.0),
				(int32_t)(sensor_input.pitch*1000.0),
				(int32_t)(sensor_input.yaw  *1000.0));
		//continue;
#endif

		if (remote_read()) {
			led_on(&led_life);
#if 0
			APP_DBG("RF[xy]\t%04d\t%04d\t%04d\t%04d\t%d\t%d\n",
					remote_data.x1, remote_data.y1, remote_data.x2, remote_data.y2, remote_data.but_1, remote_data.but_2);
			continue;
#endif

			pid_output_altitude = remote_data.y1 > 2048 ?
						(remote_data.y1 - 2048) / 2 :
						(2048 - remote_data.y1) / 2;

			if ((pid_output_altitude < 200) && !remote_data.but_1 && !remote_data.but_2)
				sys_ctrl_reset();
		}
		else {
			led_off(&led_life);
		}

		pid_output_roll		= pid_calcu(&pid_roll, sensor_input.roll, 0); //0.0005
		pid_output_pitch	= pid_calcu(&pid_pitch, sensor_input.pitch, 0);
		pid_output_yaw		= pid_calcu(&pid_yaw, sensor_input.yaw, 0);

#if 0
		APP_DBG("PID[rpy]\t%d\t%d\t%d\n",
				(int32_t)(pid_output_roll*1000),
				(int32_t)(pid_output_pitch*1000),
				(int32_t)(pid_output_yaw*1000));
		continue;
#endif

#if 0 //DEBUG_PITCH
		pid_output_roll = pid_output_yaw = 400;
#endif

#if 0 //DEBUG_ROLL
		pid_output_pitch = pid_output_yaw = 400;
#endif

#if 0 //DEBUG_YAW
		pid_output_roll = pid_output_pitch = 400;
#endif

#if 0 //DEBUG_PITCH_ROLL
		pid_output_yaw = 400;
#endif

		motor1 = pid_output_altitude + (-pid_output_roll/2.0) + (-pid_output_pitch/2.0) + (+pid_output_yaw/2.0); //Nguoc dong ho
		motor2 = pid_output_altitude + (+pid_output_roll/2.0) + (-pid_output_pitch/2.0) + (-pid_output_yaw/2.0);
		motor3 = pid_output_altitude + (-pid_output_roll/2.0) + (+pid_output_pitch/2.0) + (-pid_output_yaw/2.0);
		motor4 = pid_output_altitude + (+pid_output_roll/2.0) + (+pid_output_pitch/2.0) + (+pid_output_yaw/2.0); //Nguoc dong ho

		if(motor1 > 999) motor1 = 999;
		else if(motor1 < 200) motor1 = 0;
		if(motor2 > 999) motor2 = 999;
		else if(motor2 < 200) motor2 = 0;
		if(motor3 > 999) motor3 = 999;
		else if(motor3 < 200) motor3 = 0;
		if(motor4 > 999) motor4 = 999;
		else if(motor4 < 200) motor4 = 0;

#if 0
		APP_DBG("M\t%03d\t%03d\t%03d\t%03d\n",
				(int32_t)(motor1),
				(int32_t)(motor2),
				(int32_t)(motor3),
				(int32_t)(motor4));
		continue;
#endif

#if 0
		motor1 = motor2 = motor3 = motor4 = MAX_THR;
#endif

#if 1
		SET_MOTOR_1(motor1);
		SET_MOTOR_2(motor2);
		SET_MOTOR_3(motor3);
		SET_MOTOR_4(motor4);
#endif

	}
}

void sys_irq_timer_10ms() {
	//led_blink_polling(&led_life);
}

void sys_irq_1ms() {

}
