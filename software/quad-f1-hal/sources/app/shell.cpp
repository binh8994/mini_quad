/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "../common/cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_io.h"
#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "inter_flash.h"
#include "../driver/led/led.h"
#include "../driver/flash_program/flash_program.h"

/*****************************************************************************
*  local declare
*****************************************************************************/
#define SHELL_BUFFER_LENGHT				(32)

struct shell_t {
	uint8_t index;
	uint8_t data[SHELL_BUFFER_LENGHT];
} shell;

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);
static int32_t shell_flash(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(uint8_t*)"reset",		shell_reset,		(uint8_t*)"reset terminal"},
	{(uint8_t*)"help",		shell_help,			(uint8_t*)"help info"},
	{(uint8_t*)"reboot",	shell_reboot,		(uint8_t*)"reboot"},
	{(uint8_t*)"flash",		shell_flash,		(uint8_t*)"internal flash"},

	/*************************************************************************/
	/* debug command */
	/*************************************************************************/

	/* End Of Table */
	{(uint8_t*)0,(pf_cmd_func)0,(uint8_t*)0}
};


/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\r\n");
		while(lgn_cmd_table[idx].cmd != (uint8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\r\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_reset();
	return 0;
}

int32_t shell_flash(uint8_t* argv) {

	switch (*(argv + 6)) {
	case 'r': {
		uint8_t buf[4];
		LOGIN_PRINT("read\r\n");
		internal_flash_read(TEST_ADDRESS, buf, 4);

		for(int i = 0; i < 4; i++) {
			LOGIN_PRINT("0x%x ", buf[i]);
		}

	}
		break;

	case 'w': {
		uint32_t rand = sys_ctrl_millis();
		LOGIN_PRINT("write %x\r\n", rand);
		uint8_t ret =internal_flash_write(TEST_ADDRESS, (uint8_t*)&rand, sizeof(uint32_t));
		LOGIN_PRINT("result: %d\r\n", ret);

	}
		break;

	default:
		LOGIN_PRINT("unkown option !\r\n");
		break;
	}

	return 0;
}

void sys_irq_shell(uint8_t* s, uint32_t len) {
	uint8_t index = 0;
	uint8_t c;
	while (index < len) {
		c = s[index++];

		if (shell.index < SHELL_BUFFER_LENGHT - 1) {
			if (c == '\r' || c == '\n') {
				shell.data[shell.index] = c;
				shell.data[shell.index + 1] = 0;

				{
					uint8_t fist_char = shell.data[0];
					switch (cmd_line_parser(lgn_cmd_table, shell.data)) {
					case CMD_SUCCESS:
						break;

					case CMD_NOT_FOUND:
						if (fist_char != '\r' &&
								fist_char != '\n') {
							LOGIN_PRINT("cmd unknown\r\n");
						}
						break;

					case CMD_TOO_LONG:
						LOGIN_PRINT("cmd too long\r\n");
						break;

					case CMD_TBL_NOT_FOUND:
						LOGIN_PRINT("cmd table not found\r\n");
						break;

					default:
						LOGIN_PRINT("cmd error\r\n");
						break;
					}

					LOGIN_PRINT("#");
				}

				shell.index = 0;
			}
			else if (c == 8) {
				if (shell.index) {
					shell.index--;
				}
			}
			else {
				shell.data[shell.index++] = c;
			}
		}
		else {
			APP_PRINT("\nerror: cmd too long, cmd size: %d, try again !\r\n", SHELL_BUFFER_LENGHT);
			shell.index = 0;
		}
	}
}
