#ifndef __MPID_H__
#define __MPID_H__

typedef struct {
	float p_gain;
	float i_gain;
	float d_gain;
	int max_output;

	float p_mem;
	float i_mem;
	float d_mem;
	float last_d_error;
} mpid_t;

extern void pid_setup(mpid_t* pid, float p, float i, float d, float max);
extern float pid_calcu(mpid_t* pid,  float input, float setpoint);

#endif // __MPID_H__
