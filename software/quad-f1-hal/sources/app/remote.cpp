#include "../app/app_dbg.h"
#include "../sys/sys_io.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_arduino.h"

#include "remote.h"
#include "../rf_protocols/RF24/RF24.h"

#define RF_CHANNEL			120

static const uint64_t RF_ADDR		= {0xE88EF00FE1LL};

RF24 radio(1, 2);

static uint8_t read_tmp[sizeof(remote_data_t)];
remote_data_t remote_data;

void sys_irq_nrf24l01() {
#if 0
	bool tx_ok, tx_fail, rx_ready;
	radio.whatHappened(tx_ok, tx_fail, rx_ready);

	if(radio.available()){
	//if (rx_ready) {
		APP_DBG("rx_ready\n");
		radio.read(read_tmp,sizeof(remote_data_t));
		if (read_tmp[0] != 0xFE) return;
		memcpy(&remote_data, read_tmp, sizeof(remote_data_t));
	}
#endif
}

void remote_init() {
	SPI.begin();
}

bool remote_config() {
	if (!radio.begin())
		return false;

	radio.maskIRQ(false, false, true);
	radio.setChannel(RF_CHANNEL);
	radio.setAutoAck(false);	
	radio.setPALevel(RF24_PA_MAX);
	radio.setDataRate(RF24_250KBPS);
	radio.setCRCLength(RF24_CRC_8);
	radio.openReadingPipe(1,RF_ADDR);
	radio.startListening();

	radio.printDetails();
	return true;
}

bool remote_read() {
	if(!radio.available()) return false;

	radio.read(read_tmp,sizeof(remote_data_t));

	if (read_tmp[0] != 0xFE) return false;

	memcpy(&remote_data, read_tmp, sizeof(remote_data_t));

	return true;
}

uint8_t get_button_test() {
	return 0;
}
