#include "../common/utils.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"
#include "../sys/sys_arduino.h"

#include "app_dbg.h"
#include "motor.h"

void motor_init() {
	io_cfg_pwm();
}

void motor_config() {
	cfg_pwm();
}
