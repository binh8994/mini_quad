/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   05/09/2016
 * @Update:
 * @AnhHH: Add io function for sth11 sensor.
 ******************************************************************************
**/
#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/*****************************************************************************
 *Pin map led life
******************************************************************************/
#define LED_LIFE_IO_PIN					(GPIO_Pin_13)
#define LED_LIFE_IO_PORT				(GPIOC)
#define LED_LIFE_IO_CLOCK				(RCC_APB2Periph_GPIOC)

/*****************************************************************************
 *Pin map nRF24l01
******************************************************************************/
#define NRF_CE_IO_PIN					(GPIO_Pin_3)
#define NRF_CE_IO_PORT					(GPIOA)
#define NRF_CE_IO_CLOCK					(RCC_APB2Periph_GPIOA)


#define NRF_CSN_IO_PIN					(GPIO_Pin_4)
#define NRF_CSN_IO_PORT					(GPIOA)
#define NRF_CSN_IO_CLOCK				(RCC_APB2Periph_GPIOA)

#define NRF_IRQ_IO_PIN					(GPIO_Pin_0)
#define NRF_IRQ_IO_PORT					(GPIOB)
#define NRF_IRQ_IO_CLOCK				(RCC_APB2Periph_GPIOB)

/****************************************************************************
 *Pin map joytick X-Y-SW
*****************************************************************************/
#define X1_ADC_PIN						(GPIO_Pin_1)
#define X1_ADC_PORT						(GPIOA)
#define X1_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define X1_ADC_CHANNEL					(ADC_Channel_1)

#define Y1_ADC_PIN						(GPIO_Pin_0)
#define Y1_ADC_PORT						(GPIOA)
#define Y1_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define Y1_ADC_CHANNEL					(ADC_Channel_0)

#define X2_ADC_PIN						(GPIO_Pin_2)
#define X2_ADC_PORT						(GPIOA)
#define X2_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOA)
#define X2_ADC_CHANNEL					(ADC_Channel_2)

#define Y2_ADC_PIN						(GPIO_Pin_1)
#define Y2_ADC_PORT						(GPIOB)
#define Y2_ADC_IO_CLOCK					(RCC_APB2Periph_GPIOB)
#define Y2_ADC_CHANNEL					(ADC_Channel_9)

#define SW1_IO_PIN						(GPIO_Pin_14)
#define SW1_IO_PORT						(GPIOC)
#define SW1_IO_CLOCK					(RCC_APB2Periph_GPIOC)

#define SW2_IO_PIN						(GPIO_Pin_15)
#define SW2_IO_PORT						(GPIOC)
#define SW2_IO_CLOCK					(RCC_APB2Periph_GPIOC)

/******************************************************************************
* led status function
*******************************************************************************/
extern void led_life_init();
extern void led_life_on();
extern void led_life_off();

/******************************************************************************
* nfr24l01 IO function
*******************************************************************************/
extern void nrf24l01_io_ctrl_init();
extern void nrf24l01_ce_low();
extern void nrf24l01_ce_high();
extern void nrf24l01_csn_low();
extern void nrf24l01_csn_high();

/******************************************************************************
* adc function
* Note: MUST be enable internal clock for adc module.
*******************************************************************************/
/* configure adc peripheral */
extern void io_cfg_adc1(void);

/* adc configure io */
extern void adc_joytick_io_cfg();
extern void adc_joytick_read(uint16_t*);

extern void button_joytick_io_cfg();
extern uint8_t button_1_joytick_read();
extern uint8_t button_2_joytick_read();

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
