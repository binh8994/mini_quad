#ifndef JOYTICK_H
#define JOYTICK_H

#define	JOYTICK_PAYLOAD_LEN		9

extern void joytick_init();
extern void joytick_read(void* out);

#endif // JOYTICK_H
