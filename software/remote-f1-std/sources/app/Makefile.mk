CFLAGS		+= -I./sources/app
CPPFLAGS	+= -I./sources/app

VPATH += sources/app

# CPP source files
SOURCES_CPP += sources/app/app.cpp
SOURCES_CPP += sources/app/shell.cpp

SOURCES_CPP += sources/app/rf.cpp
SOURCES_CPP += sources/app/joytick.cpp
