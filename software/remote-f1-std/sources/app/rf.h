#ifndef RF_H
#define RF_H

extern void rf_init();
extern bool rf_config();
extern bool rf_send(void *buffer, int len);


#endif // RF_H
