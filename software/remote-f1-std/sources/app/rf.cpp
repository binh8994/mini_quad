#include "SPI.h"
#include "../sys/sys_irq.h"
#include "../rf_protocols/RF24/RF24.h"

#include "rf.h"

#define RF_CHANNEL				120
#define MAX_RF_PAYLOAD_LEN		255

static const uint64_t RF_ADDR		= {0xE88EF00FE1LL};

RF24 radio(1, 2);
static uint8_t payload[MAX_RF_PAYLOAD_LEN];

void sys_irq_nrf24l01() {
#if 0
	bool tx_ok, tx_fail, rx_ready;
	radio.whatHappened(tx_ok, tx_fail, rx_ready);

	if (tx_ok) {
		APP_DBG("tx_ok\n");
	}

	if (tx_fail) {
		APP_DBG("tx_fail\n");
	}

	if (rx_ready) {
		APP_DBG("rx_ready\n");
	}
#endif
}

void rf_init() {
	SPI.begin();
}

bool rf_config() {
	if (!radio.begin())
		return false;

	radio.maskIRQ(false, false, false);
	radio.setChannel(RF_CHANNEL);
	radio.setAutoAck(false);
	radio.setPALevel(RF24_PA_MAX);
	radio.setCRCLength(RF24_CRC_8);
	radio.setDataRate(RF24_250KBPS);
	radio.openWritingPipe(RF_ADDR);
	radio.stopListening();

	radio.printDetails();
	return true;
}

bool rf_send(void *buffer, int len) {
	payload[0] = 0xFE;
	memcpy(&payload[1], buffer, len);

	//radio.stopListening();
	return radio.write(payload, len + 1);
}
