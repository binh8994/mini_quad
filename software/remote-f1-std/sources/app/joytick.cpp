#include "../sys/sys_io.h"
#include "app_dbg.h"
#include "joytick.h"

void joytick_init() {
	/* adc peripheral configure */
	io_cfg_adc1();
	/* joytick configure io */
	adc_joytick_io_cfg();
	button_joytick_io_cfg();
}

void joytick_read(void* out) {
	adc_joytick_read((uint16_t*) out);
	*((uint8_t*)out + 8) = ( (button_1_joytick_read() << 4) & 0xF0 ) | ( 0x0F & button_2_joytick_read() );

	APP_DBG("[%02X %02X] [%02X %02X] [%02X %02X] [%02X %02X] [%02X]\n",
			*((uint8_t*)out), *((uint8_t*)out + 1), *((uint8_t*)out + 2), *((uint8_t*)out + 3),
			*((uint8_t*)out + 4),	*((uint8_t*)out + 5), *((uint8_t*)out + 6), *((uint8_t*)out + 7), *((uint8_t*)out + 8));
}

