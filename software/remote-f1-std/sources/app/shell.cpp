/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#include <stdint.h>
#include <stdlib.h>

#include "../common/cmd_line.h"
#include "../common/utils.h"
#include "../common/xprintf.h"

#include "../sys/sys_ctrl.h"
#include "../sys/sys_io.h"
#include "../sys/sys_irq.h"
#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

#include "../driver/led/led.h"

/*****************************************************************************
*  local declare
*****************************************************************************/
#define SHELL_BUFFER_LENGHT				(32)

struct shell_t {
	uint8_t index;
	uint8_t data[SHELL_BUFFER_LENGHT];
} shell;

/*****************************************************************************/
/*  command function declare
 */
/*****************************************************************************/
static int32_t shell_reset(uint8_t* argv);
static int32_t shell_ver(uint8_t* argv);
static int32_t shell_help(uint8_t* argv);
static int32_t shell_reboot(uint8_t* argv);

/*****************************************************************************/
/*  command table
 */
/*****************************************************************************/
cmd_line_t lgn_cmd_table[] = {

	/*************************************************************************/
	/* system command */
	/*************************************************************************/
	{(const int8_t*)"reset",	shell_reset,		(const int8_t*)"reset terminal"},
	{(const int8_t*)"ver",		shell_ver,			(const int8_t*)"version info"},
	{(const int8_t*)"help",		shell_help,			(const int8_t*)"help command info"},
	{(const int8_t*)"reboot",	shell_reboot,		(const int8_t*)"reboot system"},
	/*************************************************************************/
	/* debug command */
	/*************************************************************************/
	/* End Of Table */
	{(const int8_t*)0,(pf_cmd_func)0,(const int8_t*)0}
};

/*****************************************************************************/
/*  command function definaion
 */
/*****************************************************************************/
int32_t shell_reset(uint8_t* argv) {
	(void)argv;
	xprintf("\033[2J\r");
	return 0;
}

int32_t shell_ver(uint8_t* argv) {
	(void)argv;
	return 0;
}

int32_t shell_help(uint8_t* argv) {
	uint32_t idx = 0;
	switch (*(argv + 4)) {
	default:
		LOGIN_PRINT("\nCOMMANDS INFORMATION:\n\n");
		while(lgn_cmd_table[idx].cmd != (const int8_t*)0) {
			LOGIN_PRINT("%s\n\t%s\n\n", lgn_cmd_table[idx].cmd, lgn_cmd_table[idx].info);
			idx++;
		}
		break;
	}
	return 0;
}

int32_t shell_reboot(uint8_t* argv) {
	(void)argv;
	sys_ctrl_reset();
	return 0;
}

void sys_irq_shell() {
	uint8_t c = 0;
	c = sys_ctrl_shell_get_char();
	if (shell.index < SHELL_BUFFER_LENGHT - 1) {
		if (c == '\r' || c == '\n') {
			shell.data[shell.index] = c;
			shell.data[shell.index + 1] = 0;

			{
				uint8_t fist_char = shell.data[0];
				switch (cmd_line_parser(lgn_cmd_table, shell.data)) {
				case CMD_SUCCESS:
					break;

				case CMD_NOT_FOUND:
					if (fist_char != '\r' &&
							fist_char != '\n') {
						LOGIN_PRINT("cmd unknown\r\n");
					}
					break;

				case CMD_TOO_LONG:
					LOGIN_PRINT("cmd too long\r\n");
					break;

				case CMD_TBL_NOT_FOUND:
					LOGIN_PRINT("cmd table not found\r\n");
					break;

				default:
					LOGIN_PRINT("cmd error\r\n");
					break;
				}

				LOGIN_PRINT("#");
			}

			shell.index = 0;
		}
		else if (c == 8) {
			if (shell.index) {
				shell.index--;
			}
		}
		else {
			shell.data[shell.index++] = c;
		}
	}
	else {
		APP_PRINT("\nerror: cmd too long, cmd size: %d, try again !\n", SHELL_BUFFER_LENGHT);
		shell.index = 0;
	}
}
