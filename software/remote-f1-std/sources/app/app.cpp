/* driver include */
#include "../driver/led/led.h"
#include "../sys/sys_io.h"
#include "../sys/sys_ctrl.h"

/* app include */
#include "app.h"
#include "app_dbg.h"
#include "rf.h"
#include "joytick.h"

led_t led_life;
uint8_t payload[JOYTICK_PAYLOAD_LEN];

int main_app() {

	led_init(&led_life, led_life_init, led_life_on, led_life_off);

	joytick_init();
	rf_init();
	while (!rf_config()) {
		led_on(&led_life); sys_ctrl_delay_ms(500);
		led_off(&led_life); sys_ctrl_delay_ms(500);
	}

	while (true) {

		led_on(&led_life);
		joytick_read(payload);
		rf_send(payload, JOYTICK_PAYLOAD_LEN);
		led_off(&led_life);
		sys_ctrl_delay_ms(100);
	}
}
