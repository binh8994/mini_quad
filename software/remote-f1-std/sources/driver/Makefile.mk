CFLAGS		+= -I./sources/driver/led
CFLAGS		+= -I./sources/driver/rtc

VPATH += sources/driver/led
VPATH += sources/driver/rtc

SOURCES += sources/driver/led/led.c
SOURCES += sources/driver/rtc/rtc.c
